#include "MeshLoader.h"

MeshLoader::MeshLoader()
{
}

MeshLoader::~MeshLoader()
{
}

Mesh *MeshLoader::LoadMesh(std::string path)
{
	Mesh *newMesh = new Mesh();
	Assimp::Importer importer;
	const aiScene *scene = importer.ReadFile(path, aiProcess_Triangulate
		| aiProcess_GenSmoothNormals
		| aiProcess_CalcTangentSpace
		| aiProcess_LimitBoneWeights);

	for (int i = 0; i < scene->mNumMeshes; i++)
	{
		aiMesh *mesh = scene->mMeshes[i];

		for (int j = 0; j < mesh->mNumFaces; j++)
		{
			aiFace face = mesh->mFaces[j];
			for (int k = 0; k < face.mNumIndices; k++)
				newMesh->indexes.push_back(face.mIndices[k]);
		}

		for (int j = 0; j < mesh->mNumVertices; j++)
		{
			aiVector3D *vert = &(mesh->mVertices[j]);
			aiVector3D *normal = &(mesh->mNormals[j]);
			aiVector3D *st = mesh->HasTextureCoords(0) ? &(mesh->mTextureCoords[0][j]) : &aiVector3D();
			aiVector3D *tangent = &(mesh->mTangents[j]);

			newMesh->vertices.push_back(glm::vec3(vert->x, vert->y, vert->z));
			newMesh->normals.push_back(glm::vec3(normal->x, normal->y, normal->z));
			newMesh->textureCoords.push_back(glm::vec2(st->x, st->y));
		}
	}

	return newMesh;
}