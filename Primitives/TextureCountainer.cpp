#include "TextureContainer.h"

TextureContainer::TextureContainer()
{
}

TextureContainer::~TextureContainer()
{
	DeleteTextures();
}

Texture *TextureContainer::LoadTexture(std::string path)
{
	for (auto *item : textures)
	{
		if (item->GetPath() == path)
			return item;
	}

	Texture *texture = new Texture();
	try
	{
		texture->LoadTextureMetadata(path);
		textures.push_back(texture);
		return texture;
	}
	catch (pugi::xml_parse_result result)
	{
		std::string error = result.description();
		error += ": " + path;
		Log(error);
	}

	return nullptr;
}

void TextureContainer::DeleteTextures()
{
	for (auto *texture : textures)
		delete texture;

	textures.clear();
}

Texture *TextureContainer::GetTexture(std::string path)
{
	return LoadTexture(path);
}