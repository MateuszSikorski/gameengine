#pragma once

#include "PrimitivesHeader.h"

enum FilteringType
{
	NEAREST,
	LINEAR,
	ANISOTROPY
};

class Texture
{
protected:
	std::string srcPath;
	std::string path;
	GLuint textureID;
	SDL_Surface *textureData;
	bool onGPU;
	bool mipmaping;
	FilteringType filteringType;
	float anisotropyLevel;

	void SaveFiltering(pugi::xml_node *root);
	void SaveMipmaping(pugi::xml_node *root);
	void SaveAnisotropyLevel(pugi::xml_node *root);
	void ParseNode(pugi::xml_node *node);
	void LoadMipmaping(pugi::xml_attribute *attrib);
	void LoadFilteringType(pugi::xml_attribute *attrib);
	void SetFiltering();

public:
	Texture();
	virtual ~Texture();

	void LoadTexture(std::string path);
	void LoadTextureMetadata(std::string path);
	void BindTexture(int num);
	void DisbindTexture(int num);
	void SendToGPU();
	void DeleteFromGPU();
	void SetPath(std::string path) { this->path = path; }
	void SaveTextureMeta();
	void SetMipmaping(bool value) { mipmaping = value; }
	void SetFilteringType(FilteringType type) { filteringType = type; }
	void SetAnisotropyLevel(float level) { anisotropyLevel = level; }
	void SetData(SDL_Surface *surface);

	//RenderTexture
	void GenerateRenderTexture(int format);
	void DeleteRenderTexture();
	GLuint GetTextureID() { return textureID; }

	bool GetMipmaping() { return mipmaping; }
	FilteringType GetFilteringTYpe() { return filteringType; }
	float GetAnisotropyLevel() { return anisotropyLevel; }
	std::string GetPath() { return path; }
	std::string GetMetaPath();
};
