#pragma once
#include "PrimitivesHeader.h"

class Face
{
public:
	std::vector <int> vertices;
	std::vector <int> normals;
	std::vector <int> textureCoords;

	void Parse(std::vector <std::string> &data);

	Face();
	virtual ~Face();
};

