#include "Texture.h"

Texture::Texture()
{
	onGPU = false;
	textureData = nullptr;
	mipmaping = false;
	filteringType = NEAREST;
	anisotropyLevel = 0.f;
}

Texture::~Texture()
{
	if (textureData != nullptr)
		SDL_FreeSurface(textureData);

	if(onGPU)
		glDeleteTextures(1, &textureID);
}

void Texture::LoadTexture(std::string path)
{
	if (textureData != nullptr)
		SDL_FreeSurface(textureData);

	textureData = IMG_Load(srcPath.c_str());
}

void Texture::BindTexture(int num)
{
	if (!onGPU)
		SendToGPU();

	glActiveTexture(GL_TEXTURE0 + num);
	glBindTexture(GL_TEXTURE_2D, textureID);
}

void Texture::DisbindTexture(int num)
{
	glActiveTexture(GL_TEXTURE0 + num);
	glBindTexture(GL_TEXTURE_2D, NULL);
}

void Texture::SetFiltering()
{
	if (!mipmaping && filteringType == NEAREST)
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	}
	if (!mipmaping && filteringType == LINEAR)
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	}
	if (!mipmaping && filteringType == ANISOTROPY)
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, anisotropyLevel);
	}
	if (mipmaping && filteringType == NEAREST)
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	}
	if (mipmaping && filteringType == LINEAR)
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	}
	if (mipmaping && filteringType == ANISOTROPY)
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, anisotropyLevel);
	}

	if (mipmaping)
		glGenerateMipmap(GL_TEXTURE_2D);
}

void Texture::SendToGPU()
{
	glGenTextures(1, &textureID);

	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textureData->w, textureData->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureData->pixels);
	SetFiltering();
	glBindTexture(GL_TEXTURE_2D, NULL);

	onGPU = true;
}

void Texture::DeleteFromGPU()
{
	glDeleteTextures(1, &textureID);
	onGPU = false;
}

void Texture::SaveFiltering(pugi::xml_node *root)
{
	pugi::xml_node filteringNode = root->append_child("FilteringType");
	pugi::xml_attribute filteringAttribute = filteringNode.append_attribute("type");
	if (filteringType == NEAREST)
		filteringAttribute.set_value("nearest");
	else if (filteringType == LINEAR)
		filteringAttribute.set_value("linear");
	else if (filteringType == ANISOTROPY)
		filteringAttribute.set_value("anisotropy");
}

void Texture::SaveMipmaping(pugi::xml_node *root)
{
	pugi::xml_node mipmapingNode = root->append_child("Mipmaping");
	pugi::xml_attribute mipmapingAttribute = mipmapingNode.append_attribute("value");
	if (mipmaping)
		mipmapingAttribute.set_value("true");
	else
		mipmapingAttribute.set_value("false");
}

void Texture::SaveAnisotropyLevel(pugi::xml_node *root)
{
	pugi::xml_node anisotropyNode = root->append_child("Anisotropy");
	pugi::xml_attribute anisotropyAttribute = anisotropyNode.append_attribute("level");
	anisotropyAttribute.set_value(std::to_string(anisotropyLevel).c_str());
}

void Texture::SaveTextureMeta()
{
	pugi::xml_document doc;
	pugi::xml_node root = doc.append_child("Texture");
	pugi::xml_node texturePathNode = root.append_child("File");
	pugi::xml_attribute texturePathAttribute = texturePathNode.append_attribute("path");
	texturePathAttribute.set_value(path.c_str());

	SaveFiltering(&root);
	SaveMipmaping(&root);
	SaveAnisotropyLevel(&root);

	std::vector <std::string> split;
	StringSplit(&path, '.', &split);
	std::string savePath = split[0] + ".tmd";
	doc.save_file(savePath.c_str());
}

void Texture::LoadTextureMetadata(std::string path)
{
	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file(path.c_str());
	if (result.status != pugi::status_ok)
		throw result;

	pugi::xml_node root = doc.first_child();
	for (auto node : root.children())
		ParseNode(&node);

	this->path = path;
	LoadTexture(this->path);
}

void Texture::LoadMipmaping(pugi::xml_attribute *attrib)
{
	std::string value = std::string(attrib->value());

	if (value == "true")
		mipmaping = true;
	else
		mipmaping = false;
}

void Texture::LoadFilteringType(pugi::xml_attribute *attrib)
{
	std::string value = std::string(attrib->value());

	if (value == "nearest")
		filteringType = NEAREST;
	if (value == "linear")
		filteringType = LINEAR;
	if (value == "anisotropy")
		filteringType = ANISOTROPY;
}

void Texture::ParseNode(pugi::xml_node *node)
{
	std::string name = std::string(node->name());
	if (name == "File")
		srcPath = std::string(node->first_attribute().value());
	if (name == "Anisotropy")
		anisotropyLevel = std::stof(std::string(node->first_attribute().value()));
	if (name == "Mipmaping")
		LoadMipmaping(&node->first_attribute());
	if (name == "FilteringType")
		LoadFilteringType(&node->first_attribute());
}

std::string Texture::GetMetaPath()
{
	return path;
}

void Texture::SetData(SDL_Surface *surface)
{
	DeleteFromGPU();
	SDL_FreeSurface(textureData);
	textureData = surface;
}

void Texture::GenerateRenderTexture(int format)
{
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexImage2D(GL_TEXTURE_2D, 0, format, 512, 512, 0, format, GL_UNSIGNED_BYTE, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);
	onGPU = true;
}

void Texture::DeleteRenderTexture()
{
	glDeleteTextures(1, &textureID);
}