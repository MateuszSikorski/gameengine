#include "Mesh.h"

#include <iostream>

Mesh::Mesh()
{
	onGPU = false;
}

Mesh::~Mesh()
{
	if (onGPU)
		DeleteFromGPU();
}

void Mesh::GenerateBuffers()
{
	glGenVertexArrays(1, &vao);
	glGenBuffers(3, vbo);
	glGenBuffers(1, &indexBuffer);
}

void Mesh::DeleteBuffers()
{
	glDeleteBuffers(3, vbo);
	glDeleteBuffers(1, &indexBuffer);
	glDeleteVertexArrays(1, &vao);
}

void Mesh::SendIndexes()
{
	if (indexes.size() > 0)
	{
		glBindVertexArray(vao);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indexes.size(), &indexes[0], GL_STATIC_DRAW);
	}
}

void Mesh::SendVertices()
{
	if (vertices.size() > 0)
	{
		glBindVertexArray(vao);

		glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * vertices.size(), &vertices[0], GL_STATIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	}
}

void Mesh::SendTextureCoords()
{
	if (textureCoords.size() > 0)
	{
		glBindVertexArray(vao);

		glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * textureCoords.size(), &textureCoords[0], GL_STATIC_DRAW);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
	}
}

void Mesh::SendNormals()
{
	if (normals.size() > 0)
	{
		glBindVertexArray(vao);

		glBindBuffer(GL_ARRAY_BUFFER, vbo[2]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * normals.size(), &normals[0], GL_STATIC_DRAW);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_TRUE, 0, 0);
	}
}

void Mesh::EnableArrays()
{
	glBindVertexArray(vao);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
}

void Mesh::DisableArrays()
{
	glBindVertexArray(vao);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
}

void Mesh::SendToGPU()
{
	GenerateBuffers();
	SendIndexes();
	SendVertices();
	SendTextureCoords();
	SendNormals();
	EnableArrays();

	onGPU = true;
}

void Mesh::DeleteFromGPU()
{
	DisableArrays();
	DeleteBuffers();

	onGPU = false;
}

void Mesh::Render()
{
	if (!onGPU)
		SendToGPU();

	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, indexes.size(), GL_UNSIGNED_INT, 0);
}