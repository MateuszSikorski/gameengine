#include "Material.h"

Material::Material()
{
	shader = nullptr;
}

Material::~Material()
{
}

void Material::SendData()
{
	for (int i = 0; i < intList.size(); i++)
		shader->Send(intList[i].location, intList[i].elementValue);

	for (int i = 0; i < floatList.size(); i++)
		shader->Send(floatList[i].location, floatList[i].elementValue);

	for (int i = 0; i < vec2List.size(); i++)
		shader->Send(vec2List[i].location, vec2List[i].elementValue);

	for (int i = 0; i < vec3List.size(); i++)
		shader->Send(vec3List[i].location, vec3List[i].elementValue);

	for (int i = 0; i < vec4List.size(); i++)
		shader->Send(vec4List[i].location, &vec4List[i].elementValue);

	for (int i = 0; i < textureList.size(); i++)
	{
		if(textureList[i].tex != nullptr)
			textureList[i].tex->BindTexture(i);
		else
		{
			Texture t;
			t.DisbindTexture(i);
		}
			
		shader->Send(textureList[i].location, i);
	}
}

void Material::Clear()
{
	intList.clear();
	floatList.clear();
	vec2List.clear();
	vec3List.clear();
	vec4List.clear();
}

void Material::SetElement(ShaderElement *element)
{
	switch (element->type)
	{
		case SE_INT:
		{
			IntElement e;
			e.name = element->name;
			e.location = element->location;
			e.elementValue = 0;
			intList.push_back(e);
			break;
		}

		case SE_FLOAT:
		{
			FloatElement e;
			e.name = element->name;
			e.location = element->location;
			e.elementValue = 0.f;
			floatList.push_back(e);
			break;
		}

		case SE_VEC2:
		{
			Vec2Element e;
			e.name = element->name;
			e.location = element->location;
			e.elementValue = glm::vec2(0, 0);
			vec2List.push_back(e);
			break;
		}

		case SE_VEC3:
		{
			Vec3Element e;
			e.name = element->name;
			e.location = element->location;
			e.elementValue = glm::vec3(0, 0, 0);
			vec3List.push_back(e);
			break;
		}

		case SE_VEC4:
		{
			Vec4Element e;
			e.name = element->name;
			e.location = element->location;
			e.elementValue = glm::vec4(0, 0, 0, 0);
			vec4List.push_back(e);
			break;
		}

		case SE_TEXTURE2D:
		{
			TextureElement e;
			e.name = element->name;
			e.location = element->location;
			e.tex = nullptr;
			textureList.push_back(e);
			break;
		}
	}
}

void Material::SetLists()
{
	Clear();

	if (shader != nullptr)
	{
		for (int i = 0; i < shader->GetUniformListSize(); i++)
		{
			SetElement(shader->GetUniformData(i));
		}
	}
}

void Material::AttachShader(Shader *s)
{
	shader = s;
	SetLists();
}

void Material::SetElement(std::string name, int value)
{
	for (int i = 0; i < intList.size(); i++)
	{
		if (intList[i].name == name)
			intList[i].elementValue = value;
	}
}

void Material::SetElement(std::string name, float value)
{
	for (int i = 0; i < floatList.size(); i++)
	{
		if (floatList[i].name == name)
			floatList[i].elementValue = value;
	}
}

void Material::SetElement(std::string name, glm::vec2 value)
{
	for (int i = 0; i < vec2List.size(); i++)
	{
		if (vec2List[i].name == name)
			vec2List[i].elementValue = value;
	}
}

void Material::SetElement(std::string name, glm::vec3 value)
{
	for (int i = 0; i < vec3List.size(); i++)
	{
		if (vec3List[i].name == name)
			vec3List[i].elementValue = value;
	}
}

void Material::SetElement(std::string name, glm::vec4 *value)
{
	for (int i = 0; i < vec4List.size(); i++)
	{
		if (vec4List[i].name == name)
			vec4List[i].elementValue = *value;
	}
}

void Material::SetElement(std::string name, Texture *tex)
{
	for (int i = 0; i < textureList.size(); i++)
	{
		if (textureList[i].name == name)
		{
			textureList[i].tex = tex;
		}
	}
}

void Material::ParseElement(pugi::xml_node *node, TextureContainer *textureContainer)
{
	pugi::xml_attribute attrib = node->first_attribute();

	std::string attribValue(attrib.value());
	std::string elementName(attrib.next_attribute().value());
	attrib = attrib.next_attribute();
	std::string elementValue(attrib.next_attribute().value());

	if (attribValue == "int")
		SetElement(elementName, std::stoi(elementValue));
	else if (attribValue == "float")
		SetElement(elementName, std::stof(elementValue));
	else if (attribValue == "vec2")
		SetElement(elementName, StringToVector2(&elementValue));
	else if (attribValue == "vec3")
		SetElement(elementName, StringToVector3(&elementValue));
	else if (attribValue == "vec4")
	{
		glm::vec4 v = StringToVector4(&elementValue);
		SetElement(elementName, &v);
	}
	else if (attribValue == "texture")
		SetElement(elementName, textureContainer->GetTexture(elementValue));
}

void Material::ParseNode(pugi::xml_node *node, ShaderContainer *shadersContainer, TextureContainer *textureContainer)
{
	std::string name(node->name());
	if (name == "Shader")
	{
		AttachShader(shadersContainer->GetShader(node->first_attribute().value()));
	}
	else if (name == "Element")
		ParseElement(node, textureContainer);
}

void Material::Load(std::string path, ShaderContainer *shadersContainer, TextureContainer *textureContainer)
{
	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file(path.c_str());
	if (result.status != pugi::status_ok)
		throw result;

	this->path = path;
	pugi::xml_node root = doc.first_child();
	for (auto node : root.children())
		ParseNode(&node, shadersContainer, textureContainer);
}

void Material::SaveElement(pugi::xml_node *root, std::string *name, std::string *type, std::string *value)
{
	pugi::xml_node elementNode = root->append_child("Element");
	pugi::xml_attribute elementAttrib = elementNode.append_attribute("type");
	elementAttrib.set_value(type->c_str());
	elementAttrib = elementNode.append_attribute("name");
	elementAttrib.set_value(name->c_str());
	elementAttrib = elementNode.append_attribute("value");
	elementAttrib.set_value(value->c_str());
}

void Material::SaveInts(pugi::xml_node *root)
{
	for (auto element : intList)
	{
		std::string type = "int";
		std::string value = std::to_string(element.elementValue);
		SaveElement(root, &element.name, &type, &value);
	}
}

void Material::SaveFloats(pugi::xml_node *root)
{
	for (auto element : floatList)
	{
		std::string type = "float";
		std::string value = std::to_string(element.elementValue);
		SaveElement(root, &element.name, &type, &value);
	}
}

void Material::SaveVec2(pugi::xml_node *root)
{
	for (auto element : vec2List)
	{
		std::string type = "vec2";
		std::string value = VectorToString(element.elementValue);
		SaveElement(root, &element.name, &type, &value);
	}
}

void Material::SaveVec3(pugi::xml_node *root)
{
	for (auto element : vec3List)
	{
		std::string type = "vec3";
		std::string value = VectorToString(element.elementValue);
		SaveElement(root, &element.name, &type, &value);
	}
}
void Material::SaveVec4(pugi::xml_node *root)
{
	for (auto element : vec4List)
	{
		std::string type = "vec4";
		std::string value = VectorToString(&element.elementValue);
		SaveElement(root, &element.name, &type, &value);
	}
}

void Material::SaveTexture(pugi::xml_node *root)
{
	for (auto element : textureList)
	{
		std::string type = "texture";
		std::string value = element.tex->GetMetaPath();
		SaveElement(root, &element.name, &type, &value);
	}
}

void Material::Save()
{
	pugi::xml_document doc;
	pugi::xml_node root = doc.append_child("Material");
	pugi::xml_node shaderNode = root.append_child("Shader");
	pugi::xml_attribute shaderName = shaderNode.append_attribute("path");
	shaderName.set_value(shader->GetPath().c_str());

	SaveInts(&root);
	SaveFloats(&root);
	SaveVec2(&root);
	SaveVec3(&root);
	SaveVec4(&root);
	SaveTexture(&root);

	std::string savePath = path + ".mat";
	doc.save_file(savePath.c_str());
}