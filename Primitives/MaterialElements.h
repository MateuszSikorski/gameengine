#pragma once

#include "PrimitivesHeader.h"
#include "Texture.h"

struct MaterialElement
{
	std::string name;
	GLuint location;
};

struct IntElement : MaterialElement
{
	int elementValue;
};

struct FloatElement : MaterialElement
{
	float elementValue;
};

struct Vec2Element : MaterialElement
{
	glm::vec2 elementValue;
};

struct Vec3Element : MaterialElement
{
	glm::vec3 elementValue;
};

struct Vec4Element : MaterialElement
{
	glm::vec4 elementValue;
};

struct TextureElement : MaterialElement
{
	Texture *tex;
};