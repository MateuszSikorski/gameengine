#include "Face.h"

Face::Face()
{
}

Face::~Face()
{
}

void Face::Parse(std::vector <std::string> &data)
{
	if (data.size() == 4)
	{
		for (int i = 1; i < data.size(); i++)
		{
			std::vector <std::string> parse;
			StringSplit(&data[i], '/', &parse);
			if (parse[0] != "")
				vertices.push_back(std::stoi(parse[0]) - 1);
			if (parse[1] != "")
				textureCoords.push_back(std::stoi(parse[1]) - 1);
			if (parse[2] != "")
				normals.push_back(std::stoi(parse[2]) - 1);
		}
	}
	else if (data.size() == 5)
	{
		glm::ivec3 verticesData[4];
		for (int i = 1; i < data.size(); i++)
		{
			std::vector <std::string> parse;
			StringSplit(&data[i], '/', &parse);
			if (parse[0] != "")
				verticesData[i - 1].x = std::stoi(parse[0]) - 1;
			if (parse[1] != "")
				verticesData[i - 1].y = std::stoi(parse[1]) - 1;
			if (parse[2] != "")
				verticesData[i - 1].z = std::stoi(parse[2]) - 1;
		}

		vertices.push_back(verticesData[0].x);
		textureCoords.push_back(verticesData[0].y);
		normals.push_back(verticesData[0].z);
		vertices.push_back(verticesData[1].x);
		textureCoords.push_back(verticesData[1].y);
		normals.push_back(verticesData[1].z);
		vertices.push_back(verticesData[2].x);
		textureCoords.push_back(verticesData[2].y);
		normals.push_back(verticesData[2].z);

		vertices.push_back(verticesData[2].x);
		textureCoords.push_back(verticesData[2].y);
		normals.push_back(verticesData[2].z);
		vertices.push_back(verticesData[3].x);
		textureCoords.push_back(verticesData[3].y);
		normals.push_back(verticesData[3].z);
		vertices.push_back(verticesData[0].x);
		textureCoords.push_back(verticesData[0].y);
		normals.push_back(verticesData[0].z);
	}
}