#pragma once

#include "PrimitivesHeader.h"
#include "MaterialElements.h"
#include "TextureContainer.h"

class Material
{
protected:
	std::string path;
	Shader *shader;

	void SetLists();
	void SetElement(ShaderElement *element);

	void SaveElement(pugi::xml_node *root, std::string *name, std::string *type, std::string *value);

	void SaveInts(pugi::xml_node *root);
	void SaveFloats(pugi::xml_node *root);
	void SaveVec2(pugi::xml_node *root);
	void SaveVec3(pugi::xml_node *root);
	void SaveVec4(pugi::xml_node *root);
	void SaveTexture(pugi::xml_node *root);

	void ParseNode(pugi::xml_node *node, ShaderContainer *shadersCountainer, TextureContainer *textureCountainer);
	void ParseElement(pugi::xml_node *node, TextureContainer *textureCountainer);

public:
	Material();
	virtual ~Material();

	std::vector <IntElement> intList;
	std::vector <FloatElement> floatList;
	std::vector <Vec2Element> vec2List;
	std::vector <Vec3Element> vec3List;
	std::vector <Vec4Element> vec4List;
	std::vector <TextureElement> textureList;

	void AttachShader(Shader *s);
	void UseShader() { shader->Use(); }
	void TurnOffShader() { shader->TurnOff(); }
	void SendData();
	void Load(std::string path, ShaderContainer *shadersCountainer, TextureContainer *textureCountainer);
	void Save();
	void Clear();
	void SetPath(std::string path) { this->path = path; }

	void SetElement(std::string name, int value);
	void SetElement(std::string name, float value);
	void SetElement(std::string name, glm::vec2 value);
	void SetElement(std::string name, glm::vec3 value);
	void SetElement(std::string name, glm::vec4 *value);
	void SetElement(std::string name, Texture *tex);

	std::string GetPath() { return path; }

	Shader* GetShader() { return shader; }
};

