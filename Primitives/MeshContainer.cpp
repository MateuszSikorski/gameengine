#include "MeshContainer.h"

MeshContainer::MeshContainer()
{
}

MeshContainer::~MeshContainer()
{
}

Mesh *MeshContainer::LoadMesh(std::string path)
{
	for (auto *mesh : meshes)
	{
		if (mesh->GetPath() == path)
			return mesh;
	}

	Mesh *newMesh;
	MeshLoader loader;
	newMesh = loader.LoadMesh(path);
	newMesh->SetPath(path);
	meshes.push_back(newMesh);
	return newMesh;
}

void MeshContainer::DeleteMeshes()
{
	for (auto *mesh : meshes)
		delete mesh;

	meshes.clear();
}

Mesh *MeshContainer::GetMesh(std::string path)
{
	return LoadMesh(path);
}