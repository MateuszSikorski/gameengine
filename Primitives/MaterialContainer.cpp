#include "MaterialContainer.h"

MaterialContainer::MaterialContainer()
{
	shadersContainer = nullptr;
}

MaterialContainer::~MaterialContainer()
{
	DeleteMaterials();
}

void MaterialContainer::DeleteMaterials()
{
	for (auto *material : materials)
		delete material;

	materials.clear();
}

Material *MaterialContainer::LoadMaterial(std::string path)
{
	for (auto *item : materials)
	{
		if (item->GetPath() == path)
			return item;
	}

	Material *material = new Material();
	try
	{
		material->Load(path, shadersContainer, textureContainer);
		materials.push_back(material);
		return material;
	}
	catch (pugi::xml_parse_result result)
	{
		std::string error = result.description();
		error += ": " + path;
		Log(error);
	}

	return nullptr;
}

Material *MaterialContainer::GetMaterial(std::string path)
{
	return LoadMaterial(path);
}