#pragma once
#include "PrimitivesHeader.h"
#include "Mesh.h"

class MeshLoader
{
private:

public:
	MeshLoader();
	virtual ~MeshLoader();

	Mesh *LoadMesh(std::string path);
};

