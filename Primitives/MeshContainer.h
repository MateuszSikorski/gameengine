#pragma once
#include "Mesh.h"
#include "MeshLoader.h"

class MeshContainer
{
	std::vector <Mesh*> meshes;

	Mesh *LoadMesh(std::string path);

public:
	MeshContainer();
	virtual ~MeshContainer();

	void DeleteMeshes();

	Mesh *GetMesh(std::string path);
};

