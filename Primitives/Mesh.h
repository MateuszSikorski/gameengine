#pragma once

#include "PrimitivesHeader.h"

class Mesh
{
protected:
	GLuint vao;
	GLuint vbo[3];
	GLuint indexBuffer;

	std::string path;

	bool onGPU;

	void GenerateBuffers();
	void DeleteBuffers();
	void SendIndexes();
	void SendVertices();
	void SendTextureCoords();
	void SendNormals();
	void EnableArrays();
	void DisableArrays();

public:
	std::vector <unsigned int> indexes;
	std::vector <glm::vec3> vertices;
	std::vector <glm::vec3> normals;
	std::vector <glm::vec2> textureCoords;;

	Mesh();
	virtual ~Mesh();

	void SendToGPU();
	void DeleteFromGPU();
	void SetPath(std::string path){ this->path = path; }

	std::string GetPath(){ return path; }

	virtual void Render();

	bool GetOnGPU(){ return onGPU; }
};

