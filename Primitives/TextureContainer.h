#pragma once

#include "Texture.h"

class TextureContainer
{
private:
	std::vector <Texture*> textures;

	Texture *LoadTexture(std::string path);

public:
	TextureContainer();
	virtual ~TextureContainer();

	void DeleteTextures();

	Texture *GetTexture(std::string path);
};

