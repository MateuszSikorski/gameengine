#pragma once

#include "Material.h"

class MaterialContainer
{
private:
	std::vector <Material*> materials;
	ShaderContainer *shadersContainer;
	TextureContainer *textureContainer;

	Material *LoadMaterial(std::string path);

public:
	MaterialContainer();
	virtual ~MaterialContainer();

	void DeleteMaterials();
	void AttachShaderCountainer(ShaderContainer *container) { shadersContainer = container; }
	void AttachTextureCountainer(TextureContainer *container) { textureContainer = container; }

	Material *GetMaterial(std::string path);
};

