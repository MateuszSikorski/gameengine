#include "Mouse.h"

Mouse::Mouse()
{
	x = y = 0;

	for (int i = 0; i < MOUSE_BUTTONS_COUNT; i++)
		buttons[i] = M_STATE_NOTPRESSED;
}

Mouse::~Mouse()
{
}


void Mouse::Update()
{
	for (int i = 0; i < MOUSE_BUTTONS_COUNT; i++)
	{
		if (i == SDL_GetMouseState(&x, &y) - 1)
		{
			switch (buttons[i])
			{
			case M_STATE_DOWN:
				buttons[i] = M_STATE_PRESSED;
				break;
			case M_STATE_NOTPRESSED:
				buttons[i] = M_STATE_DOWN;
				break;
			}
		}
		else
		{
			switch (buttons[i])
			{
			case M_STATE_PRESSED:
				buttons[i] = M_STATE_UP;
				break;
			case M_STATE_UP:
				buttons[i] = M_STATE_NOTPRESSED;
				break;
			}
		}
	}
}

bool Mouse::ButtonDown(MouseButtons button)
{
	if (buttons[button] == M_STATE_DOWN)
		return true;

	return false;
}

bool Mouse::ButtonUp(MouseButtons button)
{
	if (buttons[button] == M_STATE_UP)
		return true;

	return false;
}

bool Mouse::ButtonPressed(MouseButtons button)
{
	if (buttons[button] == M_STATE_PRESSED)
		return true;

	return false;
}

glm::vec2 Mouse::GetPositionPixels()
{
	return glm::vec2(x, y);
}