#pragma once

#include "InputHeader.h"

#define MOUSE_BUTTONS_COUNT 3

enum MouseButtons
{
	M_LEFT,
	M_MIDDLE,
	M_RIGHT
};

enum MouseButtonState
{
	M_STATE_DOWN,
	M_STATE_UP,
	M_STATE_PRESSED,
	M_STATE_NOTPRESSED
};

class Mouse
{
private:
	int x, y;
	MouseButtonState buttons[MOUSE_BUTTONS_COUNT];

public:
	Mouse();
	virtual ~Mouse();

	void Update();

	bool ButtonDown(MouseButtons button);
	bool ButtonUp(MouseButtons button);
	bool ButtonPressed(MouseButtons button);

	glm::vec2 GetPositionPixels();
};

