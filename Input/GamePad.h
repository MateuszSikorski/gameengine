#pragma once

#include "InputHeader.h"
#define GP_BUTTON_COUNT 15

enum GamePadButtons
{
	GP_A = SDL_CONTROLLER_BUTTON_A,
	GP_B = SDL_CONTROLLER_BUTTON_B,
	GP_X = SDL_CONTROLLER_BUTTON_X,
	GP_Y = SDL_CONTROLLER_BUTTON_Y,
	GP_BACK = SDL_CONTROLLER_BUTTON_BACK,
	GP_GUIDE = SDL_CONTROLLER_BUTTON_GUIDE,
	GP_START = SDL_CONTROLLER_BUTTON_START,
	GP_LS = SDL_CONTROLLER_BUTTON_LEFTSTICK,
	GP_RS = SDL_CONTROLLER_BUTTON_RIGHTSTICK,
	GP_LB = SDL_CONTROLLER_BUTTON_LEFTSHOULDER,
	GP_RB = SDL_CONTROLLER_BUTTON_RIGHTSHOULDER,
	GP_UP = SDL_CONTROLLER_BUTTON_DPAD_UP,
	GP_DOWN = SDL_CONTROLLER_BUTTON_DPAD_DOWN,
	GP_LEFT = SDL_CONTROLLER_BUTTON_DPAD_LEFT,
	GP_RIGHT = SDL_CONTROLLER_BUTTON_DPAD_RIGHT
};

enum GamePadButtonState
{
	GP_STATE_DOWN,
	GP_STATE_UP,
	GP_STATE_PRESSED,
	GP_STATE_NOTPRESSED
};

class GamePad
{
private:
	SDL_GameController *gameController;
	std::string name;

	GamePadButtonState buttons[GP_BUTTON_COUNT];

	void ClearButtons();
	float GetAxisValue(SDL_GameControllerAxis axis);

public:
	GamePad();
	GamePad(SDL_GameController * controller);
	virtual ~GamePad();

	void Close();
	void Update();

	std::string GetName() { return name; }

	float GetLeftAxisX();
	float GetLeftAxisY();
	float GetRightAxisX();
	float GetRightAxisY();
	float GetLeftTrigger();
	float GetRightTrigger();

	bool ButtonPressed(GamePadButtons button);
	bool ButtonDown(GamePadButtons button);
	bool ButtonUp(GamePadButtons button);
};
