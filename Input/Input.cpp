#include "Input.h"

Input::Input()
{
	for (int i = 0; i < GAMEPADS_COUNT; i++)
		gamePads[i] = nullptr;
}

Input::~Input()
{
}

Input &Input::GetInstance()
{
	static Input instance;
	return instance;
}

void Input::CheckGamePadConnection()
{
	std::vector <SDL_GameController*> controllers;
	for (int i = 0; i < SDL_NumJoysticks(); i++)
	{
		if (SDL_IsGameController(i))
			controllers.push_back(SDL_GameControllerOpen(i));
	}

	for (int i = 0; i < GAMEPADS_COUNT && i < controllers.size(); i++)
	{
		gamePads[i] = new GamePad(controllers[i]);
	}
}

void Input::Update()
{
	mouse.Update();

	for (int i = 0; i < GAMEPADS_COUNT; i++)
	{
		if(gamePads[i] != nullptr)
			gamePads[i]->Update();
	}
}

void Input::Close()
{
	for (int i = 0; i < GAMEPADS_COUNT; i++)
		delete gamePads[i];
}

GamePad *Input::GetGamePad(int id)
{
	if (id >= 0 && id < GAMEPADS_COUNT)
		return gamePads[id];

	return nullptr;
}