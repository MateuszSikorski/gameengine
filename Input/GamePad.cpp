#include "GamePad.h"

GamePad::GamePad()
{
	gameController = nullptr;
	ClearButtons();
}

GamePad::GamePad(SDL_GameController * controller)
{
	gameController = controller;
	if (gameController)
		name = SDL_GameControllerName(gameController);
	ClearButtons();
}

GamePad::~GamePad()
{
	Close();
}

void GamePad::ClearButtons()
{
	for (int i = 0; i < GP_BUTTON_COUNT; i++)
	{
		buttons[i] = GP_STATE_NOTPRESSED;
	}
}

void GamePad::Close()
{
	if (gameController)
		SDL_GameControllerClose(gameController);

	gameController = nullptr;
}

void GamePad::Update()
{
	for (int i = 0; i < GP_BUTTON_COUNT; i++)
	{
		if (SDL_GameControllerGetButton(gameController, (SDL_GameControllerButton)i))
		{
			switch (buttons[i])
			{
			case GP_STATE_DOWN:
				buttons[i] = GP_STATE_PRESSED;
				break;
			case GP_STATE_NOTPRESSED:
				buttons[i] = GP_STATE_DOWN;
				break;
			}
		}
		else
		{
			switch (buttons[i])
			{
			case GP_STATE_PRESSED:
				buttons[i] = GP_STATE_UP;
				break;
			case GP_STATE_UP:
				buttons[i] = GP_STATE_NOTPRESSED;
				break;
			}
		}
	}
}

float GamePad::GetAxisValue(SDL_GameControllerAxis axis)
{
	float value = SDL_GameControllerGetAxis(gameController, axis);
	value /= 32768.f;

	return value;
}

float GamePad::GetLeftAxisX()
{
	return GetAxisValue(SDL_CONTROLLER_AXIS_LEFTX);
}

float GamePad::GetLeftAxisY()
{
	return GetAxisValue(SDL_CONTROLLER_AXIS_LEFTY);
}

float GamePad::GetRightAxisX()
{
	return GetAxisValue(SDL_CONTROLLER_AXIS_RIGHTX);
}

float GamePad::GetRightAxisY()
{
	return GetAxisValue(SDL_CONTROLLER_AXIS_RIGHTY);
}

float GamePad::GetLeftTrigger()
{
	return GetAxisValue(SDL_CONTROLLER_AXIS_TRIGGERLEFT);
}

float GamePad::GetRightTrigger()
{
	return GetAxisValue(SDL_CONTROLLER_AXIS_TRIGGERRIGHT);
}

bool GamePad::ButtonPressed(GamePadButtons button)
{
	if (buttons[button] == GP_STATE_PRESSED)
		return true;

	return false;
}

bool GamePad::ButtonDown(GamePadButtons button)
{
	if(buttons[button] == GP_STATE_DOWN)
		return true;

	return false;
}

bool GamePad::ButtonUp(GamePadButtons button)
{
	if (buttons[button] == GP_STATE_UP)
		return true;

	return false;
}