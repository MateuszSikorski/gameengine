#pragma once

#include "GamePad.h"
#include "Mouse.h"
#define GAMEPADS_COUNT 4

class Input
{
private:
	GamePad *gamePads[GAMEPADS_COUNT];
	Mouse mouse;

	Input();

public:
	virtual ~Input();

	static Input &GetInstance();

	void CheckGamePadConnection();
	void Update();
	void Close();
	
	GamePad *GetGamePad(int id);
	Mouse *GetMouse() { return &mouse; }
};

