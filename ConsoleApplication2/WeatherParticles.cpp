#include "WeatherParticles.h"

WeatherParticles::WeatherParticles()
{
	useGPGPU = false;
	updateTime = 0.f;
	try
	{
		gpgpuEngine.GetPlatformsAndDevices();
		gpgpuEngine.SetCurrentPlatform(0);
		gpgpuEngine.SetCurrentDevice(0);
		gpgpuEngine.Init();
		gpgpuEngine.LoadProgram("Particles2.gpgpu");
		gpgpuEngine.SetCurrentProgram("particlesProcedure");
	}
	catch (GPGPUerror error)
	{
		std::cout << error.description << std::endl;
	}
}

WeatherParticles::~WeatherParticles()
{
}

void WeatherParticles::Update()
{
}

void WeatherParticles::PostUpdate()
{
	Uint32 startUpdateTime = SDL_GetTicks();
	float deltaTime = GlobalTime::GetInstance().DeltaTime();
	int count = particlesPositions.size();

	glm::vec3 gravityVelocity = envirnoment->gravity * deltaTime;
	glm::vec3 windVelocity = (envirnoment->GetWindForce(surface, drag) * deltaTime) / mass;

	if (useGPGPU)
	{
		try
		{
			std::vector <int> N;
			N.push_back(particlesPositions.size());
			gpgpuEngine.SetProgramData(N, "N");
			gpgpuEngine.SetProgramData(particlesPositions, "position");
			gpgpuEngine.SetProgramData(particlesVelocity, "velocity");
			std::vector <glm::vec4> gv;
			gv.push_back(glm::vec4(gravityVelocity, 0.f));
			gpgpuEngine.SetProgramData(gv, "gravityVelocity");
			std::vector <glm::vec4> wv;
			wv.push_back(glm::vec4(windVelocity, 0.f));
			gpgpuEngine.SetProgramData(wv, "windVelocity");
			std::vector <float> t;
			t.push_back(deltaTime);
			gpgpuEngine.SetProgramData(t, "time");
			std::vector <float> rc;
			rc.push_back(drag);
			gpgpuEngine.SetProgramData(rc, "resistanceDrag");
			std::vector <float> s;
			s.push_back(surface);
			gpgpuEngine.SetProgramData(s, "surface");
			std::vector <float> d;
			d.push_back(envirnoment->dencity);
			gpgpuEngine.SetProgramData(d, "dencity");

			gpgpuEngine.SetProgramData(particlesPositions, "outPosition");
			gpgpuEngine.SetProgramData(particlesVelocity, "outVelocity");
			gpgpuEngine.Work(N[0]);
			gpgpuEngine.GetProgramData(particlesPositions, "outPosition");
			gpgpuEngine.GetProgramData(particlesVelocity, "outVelocity");
			
			gpgpuEngine.ReleaseData();
		}
		catch (GPGPUerror error)
		{
			std::cout << error.description << std::endl;
		}
	}
	else
	{
		for (int i = 0; i < count; i++)
		{
			particlesLifeTime[i] -= deltaTime;
			particlesVelocity[i] = particlesVelocity[i] + glm::vec4(gravityVelocity, 0.f) + (envirnoment->AerodynamicDrag(drag, surface, &particlesVelocity[i]) * deltaTime);
			particlesPositions[i] += (particlesVelocity[i] + glm::vec4(windVelocity, 0.f)) * deltaTime;
		}
	}

	if (CanEmitParticle())
		EmitParticle();

	SendToRender();

	Uint32 endUpdateTime = SDL_GetTicks();
	Uint32 updateTimeInt = endUpdateTime - startUpdateTime;
	double updateTimeDouble = (double)updateTimeInt / (double)1000;
	updateTime = (float)updateTimeInt;
}