#include "Grid.h"

Grid::Grid()
{
}

Grid::~Grid()
{
}

void Grid::Update()
{
}

void Grid::PostUpdate()
{
}

void Grid::CreateGrid(float s, int cells)
{
	size = s;
	cellOnSide = cells;
	length = size / (float)cellOnSide;
	radius = length * 0.77f;
	positions.resize(cellOnSide * cellOnSide * cellOnSide);
	windoNormals.resize(cellOnSide * cellOnSide * cellOnSide);

	for (int x = 0; x < cellOnSide; x++)
	{
		for (int y = 0; y < cellOnSide; y++)
		{
			for (int z = 0; z < cellOnSide; z++)
			{
				glm::vec3 pos;
				pos.x = (-size * 0.5f) + (x * length) + (length * 0.5f);
				pos.y = (-size * 0.5f) + (y * length) + (length * 0.5f);
				pos.z = (-size * 0.5f) + (z * length) + (length * 0.5f);
				positions[x + y * cellOnSide + z * cellOnSide * cellOnSide] = glm::vec4(pos, 1.f);
				pos = glm::normalize(glm::vec3(0,0,0) - pos);
				glm::vec3 cross = glm::cross(glm::vec3(0,1,0), pos);
				windoNormals[x + y * cellOnSide + z * cellOnSide * cellOnSide] = glm::vec4(cross, 0.f);
			}
		}
	}
}

glm::vec3 Grid::GetWindForce(float surface, float resistanceDrag)
{
	return glm::vec3(0, 0, 0);
}

int Grid::GetCellIndex(glm::vec4 &pos)
{
	for (int i = 0; i < positions.size(); i++)
	{
		float distance = glm::distance(pos, positions[i]);
		if (distance < radius)
			return i;
	}

	return -1;
}