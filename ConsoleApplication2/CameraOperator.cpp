#include "CameraOperator.h"
#include <iostream>

CameraOperator::CameraOperator()
{
	pad = Input::GetInstance().GetGamePad(0);
}

CameraOperator::~CameraOperator()
{
}

void CameraOperator::Update()
{
	if (pad != nullptr)
	{
		float deltaTime = GlobalTime::GetInstance().DeltaTime();
		float value = pad->GetRightAxisX();
		float value2 = pad->GetRightAxisY();
		transform.SetGlobalRotation(glm::rotate(transform.GetGlobalRotation(), value * deltaTime, glm::vec3(0, 1, 0)));
		transform.SetGlobalRotation(glm::rotate(transform.GetGlobalRotation(), value2 * deltaTime, glm::vec3(1, 0, 0)));
	}
}

void CameraOperator::PostUpdate()
{
}