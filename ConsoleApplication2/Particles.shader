<header>
#version 330 core

uniform mat4 PM;
uniform mat4 MVM;

<vertex>
layout(location = 0) in vec4 vertex;

uniform float particleSize;

out float size;

void main()
{
	size = particleSize;
	gl_Position = MVM * vertex;
}
</vertex>

<geometry>
layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

in float size[1];

out vec2 UV;

void main()
{
	vec4 position = gl_in[0].gl_Position;
	float particleSize = size[0] * 0.5;
	
	vec2 pos = position.xy + vec2(-particleSize, -particleSize);
	gl_Position = PM * vec4(pos, position.zw);
	UV = vec2(0.0, 0.0);
    EmitVertex();
	
	pos = position.xy + vec2(particleSize, -particleSize);
	gl_Position = PM * vec4(pos, position.zw);
	UV = vec2(1.0, 0.0);
    EmitVertex();
	
	pos = position.xy + vec2(-particleSize,  particleSize);
	gl_Position = PM * vec4(pos, position.zw);
	UV = vec2(0.0, 1.0);
    EmitVertex();
	
	pos = position.xy + vec2(particleSize,  particleSize);
	gl_Position = PM * vec4(pos, position.zw);
	UV = vec2(1.0, 1.0);
    EmitVertex();
    EndPrimitive();
}
</geometry>

<fragment>
in vec2 UV;

uniform sampler2D textureSampler;

out vec4 outColor;

void main()
{
	vec4 texel = texture(textureSampler, UV);
	outColor = texel;
}
</fragment>