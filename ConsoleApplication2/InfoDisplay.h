#pragma once
#include "Header.h"
#include "WeatherParticles.h"

class InfoDisplay : public GameObject
{
private:
	Text *label, *label2;
	Text *value, *value2;
	float time1000, time2000, time5000, time7500, time10000, time15000, time20000, time25000, time30000;

public:
	InfoDisplay();
	~InfoDisplay();

	float *updateTime;
	ParticleSystem *particles;

	virtual void Update();
	virtual void PostUpdate();
};

