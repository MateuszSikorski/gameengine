#include "PhysicsEnvirnoment.h"

PhysicsEnvirnoment::PhysicsEnvirnoment()
{
	gravity = glm::vec3(0.f, -9.81f, 0.f);
	wind.mainVelocity = glm::vec3(0.f);
	wind.distortion = 0.f;
	dencity = 1.184f;
}

PhysicsEnvirnoment::~PhysicsEnvirnoment()
{
}

void PhysicsEnvirnoment::Update()
{
	wind.Update();
}

void PhysicsEnvirnoment::PostUpdate()
{
}

glm::vec3 PhysicsEnvirnoment::GetWindForce(float surface, float resistanceDrag)
{
	return wind.GetWindForce(dencity, surface, resistanceDrag);
}

glm::vec4 PhysicsEnvirnoment::AerodynamicDrag(float resistanceDrag, float surface, glm::vec4 *velocity)
{
	float value = resistanceDrag * surface * dencity * 0.5f;
	glm::vec4 vel = *velocity * *velocity;
	if (*velocity != glm::vec4(0.f))
		return value * vel * -glm::normalize(*velocity);
	else
		return glm::vec4(0.f);
}