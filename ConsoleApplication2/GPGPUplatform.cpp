#include "GPGPUplatform.h"

GPGPUplatform::GPGPUplatform()
{
	platform = nullptr;
}

GPGPUplatform::GPGPUplatform(cl_platform_id *p)
{
	platform = p;
	Init();
}

GPGPUplatform::~GPGPUplatform()
{
	Destroy();
	delete platform;
}

void GPGPUplatform::Init()
{
	cl_int error;
	cl_uint deviceNumber;
	error = clGetDeviceIDs(*platform, CL_DEVICE_TYPE_ALL, 0, nullptr, &deviceNumber);
	if (error != CL_SUCCESS)
	{
		GPGPUerror e;
		e.description = "Cannot get devices";
		throw e;
	}
	cl_device_id *platformDevices = new cl_device_id[deviceNumber];
	error = clGetDeviceIDs(*platform, CL_DEVICE_TYPE_ALL, deviceNumber, platformDevices, &deviceNumber);

	for (int i = 0; i < deviceNumber; i++)
		devices.push_back(new GPGPUdevice(&platformDevices[i]));
}

void GPGPUplatform::Destroy()
{
	for (int i = 0; i < devices.size(); i++)
		delete devices[i];

	devices.clear();
}

bool GPGPUplatform::DeviceExist(uint num)
{
	if (num >= (uint)devices.size())
		return false;

	return true;
}

GPGPUdevice *GPGPUplatform::GetDevice(uint num)
{
	if (DeviceExist(num))
		return devices[num];
	else
		return nullptr;
}

std::string GPGPUplatform::GetInfo(int type)
{
	char data[DATA_SIZE];
	clGetPlatformInfo(*platform, type, DATA_SIZE, data, nullptr);

	return std::string(data);
}

std::string GPGPUplatform::GetName()
{
	return GetInfo(CL_PLATFORM_NAME);
}

std::string GPGPUplatform::GetVersion()
{
	return GetInfo(CL_PLATFORM_VERSION);
}

std::string GPGPUplatform::GetVendor()
{
	return GetInfo(CL_PLATFORM_VENDOR);
}