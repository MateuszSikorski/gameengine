#pragma once
#include "C:\Users\Mateusz\Documents\BitBucket\GameEngine v2.0\Core\ParticleSystem.h"
#include "PhysicsEnvirnoment.h"
#include "GPGPUengine.h"
#include "Grid.h"

class WeatherParticles : public ParticleSystem
{
protected:
	GPGPUengine gpgpuEngine;

public:
	bool useGPGPU;
	float mass;
	float drag;
	float surface;
	float updateTime;
	PhysicsEnvirnoment *envirnoment;

	WeatherParticles();
	virtual ~WeatherParticles();

	virtual void Update();
	virtual void PostUpdate();
};

