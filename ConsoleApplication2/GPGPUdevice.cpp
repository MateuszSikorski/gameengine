#include "GPGPUdevice.h"

GPGPUdevice::GPGPUdevice()
{
	device = nullptr;
}

GPGPUdevice::GPGPUdevice(cl_device_id *d)
{
	device = d;
}

GPGPUdevice::~GPGPUdevice()
{
	clReleaseDevice(*device);
	delete device;
}

std::string GPGPUdevice::Getinfo(int type)
{
	char data[DATA_SIZE];
	clGetDeviceInfo(*device, type, DATA_SIZE, data, nullptr);

	return std::string(data);
}

std::string GPGPUdevice::GetName()
{
	return Getinfo(CL_DEVICE_NAME);
}

std::string GPGPUdevice::GetType()
{
	cl_device_type data;
	clGetDeviceInfo(*device, CL_DEVICE_TYPE, DATA_SIZE, &data, nullptr);
	switch (data)
	{
	case CL_DEVICE_TYPE_GPU:
		return "GPU";
	case CL_DEVICE_TYPE_CPU:
		return "CPU";
	}

	return "Not identyfied";
}

std::string GPGPUdevice::GetMaxWorkItem()
{
	size_t data[128];
	clGetDeviceInfo(*device, CL_DEVICE_MAX_WORK_GROUP_SIZE, DATA_SIZE, data, nullptr);
	std::string d = "Size: " + std::to_string(data[0]);
	d += " " + std::to_string(data[1]);
	d += " " + std::to_string(data[2]);

	return d;
}
std::string GPGPUdevice::GetMaxDimensions()
{
	cl_uint data;
	clGetDeviceInfo(*device, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, DATA_SIZE, &data, nullptr);

	return std::to_string(data);
}


std::string GPGPUdevice::GetVendor()
{
	return Getinfo(CL_DEVICE_VENDOR);
}