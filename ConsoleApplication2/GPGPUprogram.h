#pragma once
#include "Header.h"
#include "GPGPUerror.h"

class GPGPUprogram
{
private:
	enum Flag
	{
		NONE,
		HEADER,
		KERNEL
	};

	enum InOut
	{
		GPGPUIN = CL_MEM_READ_ONLY,
		GPGPUOUT = CL_MEM_WRITE_ONLY
	};

	struct BufferData
	{
		InOut type;
		cl_mem buffer;
		std::string name;
	};

	Flag flag;
	std::vector <std::string> loadFileData;
	std::vector <std::string> header;
	std::vector <std::string> kernelCode;
	std::vector <BufferData> buffers;
	std::string mainFunction;
	cl_program program;
	cl_kernel kernel;
	cl_context *contextPtr;

	void ParseCode();
	void ParseHeader();

public:
	GPGPUprogram();
	~GPGPUprogram();

	void LoadSource(std::string path);
	void CreateProgram(cl_context *context, cl_int deviceNum, cl_device_id *device);
	void Work(cl_command_queue *queue, size_t globalSize);
	void SetData(std::vector <int>&data, std::string name, cl_command_queue *queue);
	void SetData(std::vector <glm::vec4>&data, std::string name, cl_command_queue *queue);
	void SetData(std::vector <float> &data, std::string name, cl_command_queue *queue);
	void GetData(std::vector <glm::vec4>&data, std::string name, cl_command_queue *queue);
	void GetData(std::vector <int>&data, std::string name, cl_command_queue *queue);
	void GetData(std::vector <float>&data, std::string name, cl_command_queue *queue);
	void ReleaseAllData();

	std::string GetMainFunctionName() { return mainFunction; }
};

