<header>
#version 330 core
struct VertexData
{
	vec2 uv;
	vec3 normal;
};

uniform mat4 PM;
uniform mat4 MVM;

<vertex>
layout(location = 0) in vec3 vertex;
layout(location = 1) in vec2 textureCoords;
layout(location = 2) in vec3 normal;

uniform vec3 color;
out VertexData dataIn;

void main()
{
	dataIn.uv = textureCoords;
	dataIn.normal = normal;
	
	gl_Position = PM * MVM * vec4(vertex, 1.0);
}
</vertex>

<geometry>
layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

void main()
{
	for(int i = 0; i < 3; i++)
	{
		gl_Position = gl_in[i].gl_Position;
		EmitVertex();
	}
	EndPrimitive();
}
</geometry>

<fragment>
in VertexData dataIn;

uniform float colorElement;

out vec4 outColor;

void main()
{
	outColor = vec4(colorElement, 0.0, 0.0, 1.0);
}
</fragment>