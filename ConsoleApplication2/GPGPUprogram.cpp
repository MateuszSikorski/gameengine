#include "GPGPUprogram.h"
#include <iostream>

GPGPUprogram::GPGPUprogram()
{
}

GPGPUprogram::~GPGPUprogram()
{
	clReleaseKernel(kernel);
	clReleaseProgram(program);
}

void GPGPUprogram::LoadSource(std::string path)
{
	std::fstream file;
	file.open(path);
	if (!file.is_open())
	{
		GPGPUerror error;
		error.description = "Could not finde file " + path;
		throw error;
	}
	std::string line;
	while (getline(file, line))
		loadFileData.push_back(line);

	file.close();

	ParseCode();
}

void GPGPUprogram::ParseHeader()
{
	std::regex patern("(\\w*)\\s*(\\w*)");
	for (int i = 0; i < header.size(); i++)
	{
		std::smatch value;
		std::regex_search(header[i], value, patern);
		if (value[1] == "main")
			mainFunction = value[2];
		else if (value[1] == "in")
		{
			BufferData data;
			data.type = GPGPUIN;
			data.name = value[2];
			buffers.push_back(data);
		}
		else if (value[1] == "out")
		{
			BufferData data;
			data.type = GPGPUOUT;
			data.name = value[2];
			buffers.push_back(data);
		}
	}
}

void GPGPUprogram::ParseCode()
{
	flag = NONE;
	for (int i = 0; i < loadFileData.size(); i++)
	{
		if (loadFileData[i] == "</kernel>" || loadFileData[i] == "</header>")
			flag = NONE;

		if (flag == HEADER)
			header.push_back(loadFileData[i]);
		if (flag == KERNEL)
			kernelCode.push_back(loadFileData[i]);

		if (loadFileData[i] == "<header>")
			flag = HEADER;
		if (loadFileData[i] == "<kernel>")
			flag = KERNEL;
	}

	ParseHeader();
	header.clear();
}

void GPGPUprogram::CreateProgram(cl_context *context, cl_int deviceNum, cl_device_id *device)
{
	GPGPUerror error;
	contextPtr = context;
	std::string code;
	for (int i = 0; i < kernelCode.size(); i++)
		code += kernelCode[i];

	char *myArray = new char[code.size() + 1];
	for (int i = 0; i < code.size(); i++)
		myArray[i] = code[i];
	myArray[code.size()] = '\0';

	cl_int errorCode;
	program = clCreateProgramWithSource(*context, 1, (const char**)&myArray, nullptr, &errorCode);
	if (errorCode != 0)
	{
		error.description = "Creating program error code: " + std::to_string(errorCode);
		throw error;
	}
	delete[] myArray;
	errorCode = clBuildProgram(program, deviceNum, device, nullptr, nullptr, nullptr);
	if (errorCode == CL_BUILD_PROGRAM_FAILURE) {
		size_t log_size;
		clGetProgramBuildInfo(program, device[0], CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
		char *log = (char *)malloc(log_size);
		clGetProgramBuildInfo(program, device[0], CL_PROGRAM_BUILD_LOG, log_size, log, NULL);
		error.description = log;
		throw error;
	}

	kernel = clCreateKernel(program, mainFunction.c_str(), &errorCode);
	kernelCode.clear();
}

void GPGPUprogram::Work(cl_command_queue *queue, size_t globalSize)
{
	clEnqueueNDRangeKernel(*queue, kernel, 1, nullptr, &globalSize, nullptr, 0, nullptr, nullptr);
}

void GPGPUprogram::SetData(std::vector <int>&data, std::string name, cl_command_queue *queue)
{
	GPGPUerror er;
	for (int i = 0; i < buffers.size(); i++)
	{
		if (buffers[i].name == name)
		{
			cl_int error;
			buffers[i].buffer = clCreateBuffer(*contextPtr, buffers[i].type, sizeof(int)*data.size(), nullptr, &error);
			if (error != 0)
			{
				er.description = "Creating buffer error code: " + std::to_string(error);
				throw er;
			}
			error = clSetKernelArg(kernel, i, sizeof(buffers[i].buffer), &buffers[i].buffer);
			if (error != 0)
			{
				er.description = std::to_string(i) + " Set kernel args error code: " + std::to_string(error);
				throw er;
			}
			if (buffers[i].type == GPGPUIN)
			{
				error = clEnqueueWriteBuffer(*queue, buffers[i].buffer, CL_FALSE, 0, sizeof(int)*data.size(), &data[0], 0, nullptr, nullptr);
				if (error != 0)
				{
					er.description = "Writing to buffer error code: " + std::to_string(error);
					throw er;
				}
			}
			return;
		}
	}

	er.description = "There is no such buffer";
	throw er;
}

void GPGPUprogram::GetData(std::vector <int>&data, std::string name, cl_command_queue *queue)
{
	for (int i = 0; i < buffers.size(); i++)
	{
		if (buffers[i].name == name && buffers[i].type == GPGPUOUT)
		{
			clEnqueueReadBuffer(*queue, buffers[i].buffer, CL_TRUE, 0, sizeof(int)*data.size(), &data[0], 0, nullptr, nullptr);
		}
	}
}

void GPGPUprogram::ReleaseAllData()
{
	for (int i = 0; i < buffers.size(); i++)
	{
		clReleaseMemObject(buffers[i].buffer);
	}
}

void GPGPUprogram::SetData(std::vector <glm::vec4>&data, std::string name, cl_command_queue *queue)
{
	GPGPUerror er;
	for (int i = 0; i < buffers.size(); i++)
	{
		if (buffers[i].name == name)
		{
			cl_int error;
			buffers[i].buffer = clCreateBuffer(*contextPtr, buffers[i].type, sizeof(glm::vec4)*data.size(), nullptr, &error);
			if (error != 0)
			{
				er.description = "Creating buffer error code: " + std::to_string(error);
				throw er;
			}
			error = clSetKernelArg(kernel, i, sizeof(buffers[i].buffer), &buffers[i].buffer);
			if (error != 0)
			{
				er.description = std::to_string(i) + " Set kernel args error code: " + std::to_string(error);
				throw er;
			}
			if (buffers[i].type == GPGPUIN)
			{
				error = clEnqueueWriteBuffer(*queue, buffers[i].buffer, CL_FALSE, 0, sizeof(glm::vec4)*data.size(), &data[0], 0, nullptr, nullptr);
				if (error != 0)
				{
					er.description = "Writing to buffer error code: " + std::to_string(error);
					throw er;
				}
			}
			return;
		}
	}

	er.description = "There is no such buffer";
	throw er;
}

void GPGPUprogram::GetData(std::vector <glm::vec4>&data, std::string name, cl_command_queue *queue)
{
	for (int i = 0; i < buffers.size(); i++)
	{
		if (buffers[i].name == name && buffers[i].type == GPGPUOUT)
		{
			clEnqueueReadBuffer(*queue, buffers[i].buffer, CL_TRUE, 0, sizeof(glm::vec4)*data.size(), &data[0], 0, nullptr, nullptr);
		}
	}
}

void GPGPUprogram::SetData(std::vector <float>&data, std::string name, cl_command_queue *queue)
{
	GPGPUerror er;
	for (int i = 0; i < buffers.size(); i++)
	{
		if (buffers[i].name == name)
		{
			cl_int error;
			buffers[i].buffer = clCreateBuffer(*contextPtr, buffers[i].type, sizeof(float)*data.size(), nullptr, &error);
			if (error != 0)
			{
				er.description = "Creating buffer error code: " + std::to_string(error);
				throw er;
			}
			error = clSetKernelArg(kernel, i, sizeof(buffers[i].buffer), &buffers[i].buffer);
			if (error != 0)
			{
				er.description = std::to_string(i) + " Set kernel args error code: " + std::to_string(error);
				throw er;
			}
			if (buffers[i].type == GPGPUIN)
			{
				error = clEnqueueWriteBuffer(*queue, buffers[i].buffer, CL_FALSE, 0, sizeof(float)*data.size(), &data[0], 0, nullptr, nullptr);
				if (error != 0)
				{
					er.description = "Writing to buffer error code: " + std::to_string(error);
					throw er;
				}
			}
			return;
		}
	}

	er.description = "There is no such buffer";
	throw er;
}

void GPGPUprogram::GetData(std::vector <float>&data, std::string name, cl_command_queue *queue)
{
	for (int i = 0; i < buffers.size(); i++)
	{
		if (buffers[i].name == name && buffers[i].type == GPGPUOUT)
		{
			clEnqueueReadBuffer(*queue, buffers[i].buffer, CL_TRUE, 0, sizeof(float)*data.size(), &data[0], 0, nullptr, nullptr);
		}
	}
}