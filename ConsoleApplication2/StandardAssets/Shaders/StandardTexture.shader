<header>
#version 330 core

uniform mat4 PM;
uniform mat4 MVM;

<vertex>
layout(location = 0) in vec3 vertex;
layout(location = 1) in vec2 textureCoords;

out vec2 UV;

void main()
{
	UV = textureCoords;
	
	gl_Position = PM * MVM * vec4(vertex, 1.0);
}
</vertex>

<fragment>
in vec2 UV;

uniform sampler2D textureSampler;

out vec4 outColor;

void main()
{
	outColor = texture(textureSampler, UV);
}
</fragment>