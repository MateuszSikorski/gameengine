#include "GPGPUengine.h"

GPGPUengine::GPGPUengine()
{
	currentPlatform = nullptr;
	currentDevice = nullptr;
	currentProgram = nullptr;
}

GPGPUengine::~GPGPUengine()
{
	Destroy();
}

bool GPGPUengine::PlatformExist(uint num)
{
	if (num >= (uint)platforms.size())
		return false;

	return true;
}

void GPGPUengine::GetPlatformsAndDevices()
{
	cl_int error;
	cl_uint platformNumber;
	error = clGetPlatformIDs(0, nullptr, &platformNumber);
	if (error != CL_SUCCESS)
	{
		GPGPUerror e;
		e.description = "Cannot get platforms";
		throw e;
	}
	cl_platform_id *platformList = new cl_platform_id[platformNumber];
	error = clGetPlatformIDs(platformNumber, platformList, nullptr);

	for (int i = 0; i < platformNumber; i++)
		platforms.push_back(new GPGPUplatform(&platformList[i]));
}

void GPGPUengine::Destroy()
{
	clReleaseCommandQueue(commandQueue);
	clReleaseContext(context);

	for (int i = 0; i < platforms.size(); i++)
		delete platforms[i];

	platforms.clear();

	for (int i = 0; i < programs.size(); i++)
		delete programs[i];

	programs.clear();
}

void GPGPUengine::SetCurrentPlatform(uint num)
{
	if (PlatformExist(num))
		currentPlatform = platforms[num];
	else
	{
		GPGPUerror error;
		error.description = "Wrong platform number";
		throw error;
	}
}

void GPGPUengine::SetCurrentDevice(uint num)
{
	GPGPUerror error;
	if (currentPlatform == nullptr)
	{
		error.description = "No platform set";
		throw error;
	}

	currentDevice = currentPlatform->GetDevice(num);
	if (currentDevice == nullptr)
	{
		error.description = "Wrong device number";
		throw error;
	}
}

void GPGPUengine::SetCurrentProgram(std::string name)
{
	for (int i = 0; i < programs.size(); i++)
	{
		if (programs[i]->GetMainFunctionName() == name)
			currentProgram = programs[i];
	}
}

GPGPUplatform* GPGPUengine::GetPlatform(uint num)
{
	if (PlatformExist(num))
		return platforms[num];
	else
		return nullptr;
}

void GPGPUengine::CheckCurrendPlatformAndDevice()
{
	GPGPUerror error;
	if (currentPlatform == nullptr)
	{
		error.description = "No platform set";
		throw error;
	}

	if (currentDevice == nullptr)
	{
		error.description = "No device set";
		throw error;
	}
}

void GPGPUengine::CreateContext()
{
	cl_int errorCode;
	context = clCreateContext(nullptr, 1, currentDevice->GetDevice(), nullptr, nullptr, &errorCode);
	if (!context)
	{
		GPGPUerror error;
		error.description = "Cannot create context. OpenCL error code: " + std::to_string(errorCode);
		throw error;
	}
}

void GPGPUengine::CreateCommandQueue()
{
	cl_int errorCode;
	commandQueue = clCreateCommandQueue(context, *currentDevice->GetDevice(), 0, &errorCode);
	if (!commandQueue)
	{
		GPGPUerror error;
		error.description = "Cannot create command queue. OpenCL error code: " + std::to_string(errorCode);
		throw error;
	}
}

void GPGPUengine::Init()
{
	try
	{
		CheckCurrendPlatformAndDevice();
		CreateContext();
		CreateCommandQueue();
	}
	catch (GPGPUerror error)
	{
		throw error;
	}
}

void GPGPUengine::LoadProgram(std::string path)
{
	try
	{
		GPGPUprogram *program = new GPGPUprogram();
		program->LoadSource(path);
		program->CreateProgram(&context, currentPlatform->GetDeviceCount(), currentDevice->GetDevice());
		programs.push_back(program);
	}
	catch (GPGPUerror error)
	{
		error.description = path + ":\n" + error.description;
		throw error;
	}
}

void GPGPUengine::SetProgramData(std::vector <int>&data, std::string name)
{
	if (currentProgram != nullptr)
	{
		try
		{
			currentProgram->SetData(data, name, &commandQueue);
		}
		catch (GPGPUerror error)
		{
			GPGPUerror er;
			er.description = name + ":\n" + error.description;
			throw er;
		}
	}
	else
	{
		GPGPUerror error;
		error.description = "Undefine current program";
		throw error;
	}
}

void GPGPUengine::GetProgramData(std::vector <int>&data, std::string name)
{
	if (currentProgram != nullptr)
		currentProgram->GetData(data, name, &commandQueue);
	else
	{
		GPGPUerror error;
		error.description = "Undefine current program";
		throw error;
	}
}

void GPGPUengine::SetProgramData(std::vector <glm::vec4>&data, std::string name)
{
	if (currentProgram != nullptr)
	{
		try
		{
			currentProgram->SetData(data, name, &commandQueue);
		}
		catch (GPGPUerror error)
		{
			GPGPUerror er;
			er.description = name + ":\n" + error.description;
			throw er;
		}
	}
	else
	{
		GPGPUerror error;
		error.description = "Undefine current program";
		throw error;
	}
}

void GPGPUengine::GetProgramData(std::vector <glm::vec4>&data, std::string name)
{
	if (currentProgram != nullptr)
		currentProgram->GetData(data, name, &commandQueue);
	else
	{
		GPGPUerror error;
		error.description = "Undefine current program";
		throw error;
	}
}

void GPGPUengine::SetProgramData(std::vector <float>&data, std::string name)
{
	if (currentProgram != nullptr)
	{
		try
		{
			currentProgram->SetData(data, name, &commandQueue);
		}
		catch (GPGPUerror error)
		{
			GPGPUerror er;
			er.description = name + ":\n" + error.description;
			throw er;
		}
	}
	else
	{
		GPGPUerror error;
		error.description = "Undefine current program";
		throw error;
	}
}

void GPGPUengine::GetProgramData(std::vector <float>&data, std::string name)
{
	if (currentProgram != nullptr)
		currentProgram->GetData(data, name, &commandQueue);
	else
	{
		GPGPUerror error;
		error.description = "Undefine current program";
		throw error;
	}
}

void GPGPUengine::Work(size_t globalSize)
{
	if (currentProgram != nullptr)
	{
		currentProgram->Work(&commandQueue, globalSize);
	}
	else
	{
		GPGPUerror error;
		error.description = "Undefine current program";
		throw error;
	}
}

void GPGPUengine::ReleaseData()
{
	currentProgram->ReleaseAllData();
}