#pragma once
#include "C:\Users\Mateusz\Documents\BitBucket\GameEngine v2.0\Core\GameObject.h"

class Wind : public GameObject
{
private:
	glm::vec3 velocity;

public:
	glm::vec3 mainVelocity;
	float distortion;

	Wind();
	virtual ~Wind();

	glm::vec3 GetVelocity() { return velocity; }
	glm::vec3 GetWindForce(float envirnometDencity, float surface, float resistanceDrag);

	virtual void Update();
	virtual void PostUpdate();
};

