#pragma once
#include "GPGPUerror.h"

class GPGPUdevice
{
private:
	cl_device_id *device;

	std::string Getinfo(int type);

public:
	GPGPUdevice();
	GPGPUdevice(cl_device_id *d);
	~GPGPUdevice();

	std::string GetType();
	std::string GetVendor();
	std::string GetName();
	std::string GetMaxWorkItem();
	std::string GetMaxDimensions();
	cl_device_id* GetDevice() { return device; }
};

