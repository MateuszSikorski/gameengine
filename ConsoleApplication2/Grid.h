#pragma once
#include "PhysicsEnvirnoment.h"

class Grid : public PhysicsEnvirnoment
{
public:
	std::vector <glm::vec4> windoNormals;
	std::vector <glm::vec4> positions;
	float radius;
	float velocity;
	float size;
	int cellOnSide;
	float length;

	Grid();
	virtual ~Grid();

	virtual void Update();
	virtual void PostUpdate();
	virtual glm::vec3 GetWindForce(float surface, float resistanceDrag);

	void CreateGrid(float s, int cells);
	int GetCellIndex(glm::vec4 &pos);
};

