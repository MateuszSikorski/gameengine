#pragma once
#include "WeatherParticles.h"
#include "Grid.h"

class WeatherParticlesZone :
	public WeatherParticles
{
public:
	Grid *grid;

	WeatherParticlesZone();
	virtual ~WeatherParticlesZone();

	virtual void Update();
	virtual void PostUpdate();
};

