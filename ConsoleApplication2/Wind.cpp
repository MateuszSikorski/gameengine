#include "Wind.h"

Wind::Wind()
{
	mainVelocity = velocity = glm::vec3(1,0,0);
	distortion = 0.5f;
}

Wind::~Wind()
{
}

glm::vec3 Wind::GetWindForce(float envirnometDencity, float surface, float resistanceDrag)
{
	float value = envirnometDencity * surface * resistanceDrag * 0.5f;
	glm::vec3 force = velocity * velocity * value;
	if (velocity != glm::vec3(0.f))
		force *= glm::normalize(velocity);

	return force;
}

void Wind::Update()
{
	float time = GlobalTime::GetInstance().GetTimeSinceStart();
	float dis = distortion * glm::sin(time);
	dis += distortion * glm::sin(time * 0.1f);
	dis += distortion * glm::sin(time * 1.2f);
	dis += distortion * glm::sin(time * 0.004f);
	dis /= 4.f;

	if (mainVelocity != glm::vec3(0, 0, 0))
		velocity = mainVelocity + dis * glm::normalize(mainVelocity);
	else
		velocity = glm::vec3(0.f);
}

void Wind::PostUpdate()
{
}