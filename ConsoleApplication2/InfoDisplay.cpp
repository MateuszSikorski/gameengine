#include "InfoDisplay.h"

InfoDisplay::InfoDisplay()
{
	layer = 2;
	Font *font = Resources::GetInstance().GetFont("Fonts/OpenSans-Regular.ttf");

	label = dynamic_cast<Text*>(Engine::GetInstance().AddObject(new Text(font)));
	transform.AddChildren(&label->transform);
	label->layer = 2;
	label->SetText("Update time:");

	label2 = dynamic_cast<Text*>(Engine::GetInstance().AddObject(new Text(font)));
	transform.AddChildren(&label2->transform);
	label2->layer = 2;
	label2->transform.SetLocalPosition(glm::vec3(0, -label->GetRect().w, 0));
	label2->SetText("Particles:");

	value = dynamic_cast<Text*>(Engine::GetInstance().AddObject(new Text(font)));
	transform.AddChildren(&value->transform);
	value->layer = 2;
	value->transform.SetLocalPosition(glm::vec3(label->GetRect().z, 0, 0));

	value2 = dynamic_cast<Text*>(Engine::GetInstance().AddObject(new Text(font)));
	transform.AddChildren(&value2->transform);
	value2->layer = 2;
	value2->transform.SetLocalPosition(glm::vec3(label2->GetRect().z, -label->GetRect().w, 0));
	value2->SetText("Off");
}

InfoDisplay::~InfoDisplay()
{
}

void InfoDisplay::Update()
{
	int pc = particles->GetParticlesCount();
	if (pc > 980 && pc < 1020)
		time1000 = (time1000 + *updateTime) * 0.5f;
	else if (pc > 1980 && pc < 2020)
		time2000 = (time2000 + *updateTime) * 0.5f;
	else if (pc > 4980 && pc < 5020)
		time5000 = (time5000 + *updateTime) * 0.5f;
	else if (pc > 7480 && pc < 7520)
		time7500 = (time7500 + *updateTime) * 0.5f;
	else if (pc > 9980 && pc < 10020)
		time10000 = (time10000 + *updateTime) * 0.5f;
	else if (pc > 14980 && pc < 15020)
		time15000 = (time15000 + *updateTime) * 0.5f;
	else if (pc > 19980 && pc < 20020)
		time20000 = (time2000 + *updateTime) * 0.5f;
	else if (pc > 24980 && pc < 25020)
		time25000 = (time25000 + *updateTime) * 0.5f;
	else if (pc > 29980 && pc < 30020)
		time30000 = (time30000 + *updateTime) * 0.5f;

	std::string s = "t1 " + std::to_string(time1000) + " t2 " + std::to_string(time2000) + " t3 " + std::to_string(time5000) + " t4 " + std::to_string(time7500) + " t5 " + std::to_string(time10000) + " t6 " + std::to_string(time15000) + " t7 " + std::to_string(time20000) + " t8 " + std::to_string(time25000) + " t9 " + std::to_string(time30000);
	value->SetText(s);
	value2->SetText(std::to_string(pc));
}

void InfoDisplay::PostUpdate()
{
}
