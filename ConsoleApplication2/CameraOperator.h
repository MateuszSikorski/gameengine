#pragma once
#include "Header.h"

class CameraOperator : public GameObject
{
private:
	GamePad *pad;

public:
	CameraOperator();
	virtual ~CameraOperator();

	virtual void Update();
	virtual void PostUpdate();
};

