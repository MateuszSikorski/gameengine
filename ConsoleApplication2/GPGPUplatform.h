#pragma once
#include "GPGPUdevice.h"

class GPGPUplatform
{
private:
	cl_platform_id *platform;
	std::vector <GPGPUdevice*> devices;

	bool DeviceExist(uint num);
	std::string GetInfo(int type);
	void Init();

public:
	GPGPUplatform();
	GPGPUplatform(cl_platform_id *p);
	~GPGPUplatform();

	void Destroy();

	uint GetDeviceCount() { return (uint)devices.size(); }
	GPGPUdevice *GetDevice(uint num);

	std::string GetName();
	std::string GetVersion();
	std::string GetVendor();
};

