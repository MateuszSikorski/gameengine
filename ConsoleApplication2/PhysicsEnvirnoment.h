#pragma once
#include "Wind.h"

class PhysicsEnvirnoment : public GameObject
{
public:
	glm::vec3 gravity;
	Wind wind;
	float dencity;

	PhysicsEnvirnoment();
	~PhysicsEnvirnoment();

	virtual void Update();
	virtual void PostUpdate();
	virtual glm::vec3 GetWindForce(float surface, float resistanceDrag);

	glm::vec4 AerodynamicDrag(float resistanceDrag, float surface, glm::vec4 *velocity);
};

