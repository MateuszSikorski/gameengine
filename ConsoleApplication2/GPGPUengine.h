#pragma once
#include "GPGPUplatform.h"
#include "GPGPUprogram.h"

class GPGPUengine
{
private:
	std::vector <GPGPUplatform*> platforms;
	GPGPUplatform *currentPlatform;
	GPGPUdevice *currentDevice;
	cl_context context;
	cl_command_queue commandQueue;
	std::vector <GPGPUprogram*> programs;
	GPGPUprogram *currentProgram;

	bool PlatformExist(uint num);
	void CheckCurrendPlatformAndDevice();
	void CreateContext();
	void CreateCommandQueue();

public:
	GPGPUengine();
	virtual ~GPGPUengine();

	void GetPlatformsAndDevices();
	void Destroy();
	void SetCurrentPlatform(uint num);
	void SetCurrentDevice(uint num);
	void SetCurrentProgram(std::string name);
	void SetProgramData(std::vector <int>&data, std::string name);
	void GetProgramData(std::vector <int>&data, std::string name);
	void SetProgramData(std::vector <float>&data, std::string name);
	void GetProgramData(std::vector <float>&data, std::string name);
	void SetProgramData(std::vector <glm::vec4>&data, std::string name);
	void GetProgramData(std::vector <glm::vec4>&data, std::string name);
	void ReleaseData();
	void Work(size_t globalSize);
	void Init();
	void LoadProgram(std::string path);

	uint GetPlatformCount() { return (uint)platforms.size(); }
	GPGPUplatform *GetPlatform(uint num);
};

