#include "Header.h"
#include "CameraOperator.h"
#include "InfoDisplay.h"
#include "WeatherParticles.h"
#include "WeatherParticlesZone.h"
#define StereoLayer 1
#define LayerUI 2
#define Width 1024
#define Height 720
#define Left 3
#define Right 4

void InitSecoundOprion(Engine &engine, Resources &resources, bool useGPGPU, int cameraOption)
{
	PhysicsEnvirnoment *envirnoment = dynamic_cast<PhysicsEnvirnoment*>(engine.AddObject(new PhysicsEnvirnoment()));
	envirnoment->wind.mainVelocity = glm::vec3(-50, 0, 0);
	envirnoment->wind.distortion = 20.f;

	WeatherParticles *weather = dynamic_cast<WeatherParticles*>(engine.AddObject(new WeatherParticles()));
	weather->SetRenderObject(new RenderParticles());
	weather->GetRenderObject()->AttachMaterial(resources.GetMaterial("Particles.mat"));
	weather->transform.SetGlobalPosition(glm::vec3(0, 4, 0));
	weather->solid = new EmmisionQuad();
	EmmisionQuad *emmision = dynamic_cast<EmmisionQuad*>(weather->solid);
	emmision->scale = glm::vec3(4, 0, 4);
	weather->envirnoment = envirnoment;
	weather->emissionTimeOffset = 0.001f;
	weather->maxParticles = 100000;
	weather->velocity = 0.f;
	weather->particleSize = 0.2f;
	weather->particleLifeTime = 10000.f;
	weather->mass = 0.00007f;
	weather->surface = 0.00002f;
	weather->drag = 0.47f;
	weather->velocity = -3;
	weather->tag = "particle";
	weather->useGPGPU = useGPGPU;
	if (cameraOption == 3)
		weather->layer = StereoLayer;

	InfoDisplay *text = dynamic_cast<InfoDisplay*>(engine.AddObject(new InfoDisplay()));
	text->transform.SetGlobalPosition(glm::vec3(0, Height, 0));
	text->layer = LayerUI;
	text->updateTime = &weather->updateTime;
	text->particles = weather;
}

void InitThirdOprion(Engine &engine, Resources &resources, bool useGPGPU, int cameraOption)
{
	PhysicsEnvirnoment *envirnoment = dynamic_cast<PhysicsEnvirnoment*>(engine.AddObject(new PhysicsEnvirnoment()));
	envirnoment->wind.distortion = 20.f;

	Grid *grid = new Grid();
	grid->CreateGrid(8, 4);
	grid->velocity = 100.f;

	WeatherParticlesZone *weather = dynamic_cast<WeatherParticlesZone*>(engine.AddObject(new WeatherParticlesZone()));
	weather->SetRenderObject(new RenderParticles());
	weather->GetRenderObject()->AttachMaterial(resources.GetMaterial("Particles.mat"));
	weather->transform.SetGlobalPosition(glm::vec3(0, 4, 0));
	weather->solid = new EmmisionQuad();
	EmmisionQuad *emmision = dynamic_cast<EmmisionQuad*>(weather->solid);
	emmision->scale = glm::vec3(4, 0, 4);
	weather->envirnoment = envirnoment;
	weather->emissionTimeOffset = 0.001f;
	weather->maxParticles = 100000;
	weather->velocity = 0.f;
	weather->particleSize = 0.2f;
	weather->particleLifeTime = 10000.f;
	weather->mass = 0.00007f;
	weather->surface = 0.00002f;
	weather->drag = 0.47f;
	weather->velocity = -3;
	weather->tag = "particle";
	weather->useGPGPU = useGPGPU;
	weather->grid = grid;
	if (cameraOption == 3)
		weather->layer = StereoLayer;

	InfoDisplay *text = dynamic_cast<InfoDisplay*>(engine.AddObject(new InfoDisplay()));
	text->transform.SetGlobalPosition(glm::vec3(0, Height, 0));
	text->layer = LayerUI;
	text->updateTime = &weather->updateTime;
	text->particles = weather;
}

void CameraOptionOne(Engine &engine)
{
	PerspectiveCamera *mainCamera = dynamic_cast<PerspectiveCamera*>(engine.AddObject(new PerspectiveCamera()));
	mainCamera->transform.SetGlobalPosition(glm::vec3(0, 0, 10));
	mainCamera->SetViewport(engine.GetViewport());
}

void CamerraOptionTwo(Engine &engine)
{
	Camera *camera = dynamic_cast<Camera*>(engine.AddObject(new StereoscopicCamera()));
	camera->SetViewport(engine.GetViewport());
	camera->transform.SetGlobalPosition(glm::vec3(0, 0, 10));
	StereoscopicCamera *cam = dynamic_cast<StereoscopicCamera*>(camera);
	cam->SetAspect();

}

void CameraOptionThree(Engine &engine, Resources &resources)
{
	FrameBuffer *fb = new FrameBuffer();
	//stereocamera============================================================
	PerspectiveCameraRT *leftCamera = dynamic_cast<PerspectiveCameraRT*>(engine.AddObject(new PerspectiveCameraRT()));
	leftCamera->transform.SetGlobalPosition(glm::vec3(0, 0, 10));
	leftCamera->SetViewport(engine.GetViewport());
	leftCamera->SetAspect();
	leftCamera->layerList[0] = StereoLayer;
	leftCamera->clearColor = glm::vec4(0.54f, 0.89f, 1.f, 0.f);
	leftCamera->tag = "Left";
	leftCamera->AttachFrameBuffer(fb);

	//normalcamera==========================================================
	PerspectiveCamera *mainCamera2 = dynamic_cast<PerspectiveCamera*>(engine.AddObject(new PerspectiveCamera()));
	mainCamera2->transform.SetGlobalPosition(glm::vec3(0, 0, 6));
	mainCamera2->SetViewport(engine.GetViewport());
	mainCamera2->layerList[0] = Left;

	//====================================================
	FrameBuffer *fb2 = new FrameBuffer();
	PerspectiveCameraRT *rightCamera = dynamic_cast<PerspectiveCameraRT*>(engine.AddObject(new PerspectiveCameraRT()));
	rightCamera->transform.SetGlobalPosition(glm::vec3(0, 0, 10));
	rightCamera->SetViewport(engine.GetViewport());
	rightCamera->SetAspect();
	rightCamera->layerList[0] = StereoLayer;
	rightCamera->clearColor = glm::vec4(1, 0, 0, 0);// glm::vec4(0.54f, 0.89f, 1.f, 0.f);
	rightCamera->tag = "right";
	rightCamera->AttachFrameBuffer(fb2);

	//normalcamera==========================================================
	PerspectiveCamera *mainCamera = dynamic_cast<PerspectiveCamera*>(engine.AddObject(new PerspectiveCamera()));
	mainCamera->transform.SetGlobalPosition(glm::vec3(0, 0, 6));
	mainCamera->SetViewport(engine.GetViewport());
	mainCamera->layerList[0] = Right;
	mainCamera->clearFlag = NOTHING;

	//soczewka============================================================
	RenderModel *renderModel = new RenderModel();
	renderModel->AttachMesh(resources.GetMesh("StandardAssets/Meshes/soczewka.obj"));
	Object *leftOcular = engine.AddObject(new Object());
	leftOcular->SetRenderObject(renderModel);
	leftOcular->GetRenderObject()->AttachMaterial(resources.GetMaterial("StandardAssets/Materials/StandardTexture.mat"));
	leftOcular->transform.SetGlobalRotation(glm::rotate(leftOcular->transform.GetGlobalRotation(), glm::radians(90.f), glm::vec3(1, 0, 0)));
	leftOcular->GetRenderObject()->GetMaterial()->textureList[0].tex = leftCamera->GetFrameBuffer()->GetRenderTexture();
	leftOcular->transform.SetGlobalPosition(glm::vec3(-2.25f,0,0));
	leftOcular->layer = Left;

	RenderModel *renderModel2 = new RenderModel();
	renderModel2->AttachMesh(resources.GetMesh("StandardAssets/Meshes/soczewka.obj"));
	Object *rightOcular = engine.AddObject(new Object());
	rightOcular->SetRenderObject(renderModel2);
	rightOcular->GetRenderObject()->AttachMaterial(resources.GetMaterial("StandardAssets/Materials/StandardTexture2.mat"));
	rightOcular->transform.SetGlobalRotation(glm::rotate(rightOcular->transform.GetGlobalRotation(), glm::radians(90.f), glm::vec3(1, 0, 0)));
	rightOcular->GetRenderObject()->GetMaterial()->textureList[0].tex = leftCamera->GetFrameBuffer()->GetRenderTexture();
	rightOcular->transform.SetGlobalPosition(glm::vec3(2.25f, 0, 0));
	rightOcular->layer = Right;
}

int main(int argc, char *argv[])
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	ClearLog();

	bool useGPGPU = false;
	std::cout << "Use GPGPU? ";
	char a;
	std::cin >> a;
	if (a == 't')
		useGPGPU = true;

	int cameraOption;
	std::cout << "\nOpcja kamery: ";
	std::cin >> cameraOption;

	int option;
	std::cout << "\nWariant: ";
	std::cin >> option;

	Engine &engine = Engine::GetInstance();
	engine.Create("Test", Width, Height, false);
	Resources &resources = Resources::GetInstance();

	engine.AddScene(new Scene("scene"));
	engine.SetCurrentScene("scene");
	switch (cameraOption)
	{
	case 1:
		CameraOptionOne(engine);
		break;
	case 2:
		CamerraOptionTwo(engine);
		break;
	case 3:
		CameraOptionThree(engine, resources);
		break;
	}

	switch (option)
	{
	case 1:
		break;
	case 2:
		InitSecoundOprion(engine, resources, useGPGPU, cameraOption);
		break;
	case 3:
		InitThirdOprion(engine, resources, useGPGPU, cameraOption);
		break;
	}
	
	//UIcamera============================================================
	OrthographicCamera *uiCamera = dynamic_cast<OrthographicCamera*>(engine.AddObject(new OrthographicCamera()));
	uiCamera->layer = LayerUI;
	uiCamera->layerList[0] = LayerUI;
	uiCamera->clearFlag = DEPTH;
	uiCamera->SetTop(Height);
	uiCamera->SetBottom(0);
	uiCamera->SetRight(Width);
	uiCamera->SetLeft(0);
	uiCamera->SetViewport(engine.GetViewport());

	SDL_Event ev;
	bool done = false;

	Input::GetInstance().CheckGamePadConnection();
	//GamePad *pad = Input::GetInstance().GetGamePad(0);

	while (!done)
	{
		Input::GetInstance().Update();
		SDL_PollEvent(&ev);
		if (ev.type == SDL_QUIT)
			done = true;

		/*if (pad)
		{
			if (pad->ButtonDown(GP_B))
				done = true;
		}*/

		engine.Work();
	}

	//delete fb;
	//delete fb2;
	Input::GetInstance().Close();
	engine.Destroy();

	return 0;
}