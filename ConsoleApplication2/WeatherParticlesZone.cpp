#include "WeatherParticlesZone.h"

WeatherParticlesZone::WeatherParticlesZone()
{
	useGPGPU = false;
	updateTime = 0.f;
	try
	{
		gpgpuEngine.GetPlatformsAndDevices();
		gpgpuEngine.SetCurrentPlatform(0);
		gpgpuEngine.SetCurrentDevice(0);
		gpgpuEngine.Init();
		gpgpuEngine.LoadProgram("Particles3.gpgpu");
		gpgpuEngine.LoadProgram("Particles4.gpgpu");
	}
	catch (GPGPUerror error)
	{
		std::cout << error.description << std::endl;
	}
}

WeatherParticlesZone::~WeatherParticlesZone()
{
	delete grid;
}

void WeatherParticlesZone::Update()
{
}

void WeatherParticlesZone::PostUpdate()
{
	Uint32 startUpdateTime = SDL_GetTicks();
	float deltaTime = GlobalTime::GetInstance().DeltaTime();
	int count = particlesPositions.size();

	glm::vec3 gravityVelocity = envirnoment->gravity * deltaTime;

	if (useGPGPU)
	{
		try
		{
			gpgpuEngine.SetCurrentProgram("particlesProcedure");
			std::vector <int> particlesCount;
			particlesCount.push_back(particlesPositions.size());
			gpgpuEngine.SetProgramData(particlesCount, "particlesCount");
			gpgpuEngine.SetProgramData(particlesPositions, "particlesPosition");
			std::vector <int> windZonesCount;
			windZonesCount.push_back(grid->positions.size());
			gpgpuEngine.SetProgramData(windZonesCount, "windZonesCount");
			gpgpuEngine.SetProgramData(grid->positions, "windZonesPositions");
			std::vector <float> windZoneRadius;
			windZoneRadius.push_back(grid->radius);
			gpgpuEngine.SetProgramData(windZoneRadius, "windZoneRadius");
			std::vector <int> windIndexes(particlesPositions.size());
			gpgpuEngine.SetProgramData(windIndexes, "windIndexes");

			gpgpuEngine.Work(particlesCount[0]);
			gpgpuEngine.GetProgramData(windIndexes, "windIndexes");

			gpgpuEngine.ReleaseData();
			//============================================================
			gpgpuEngine.SetCurrentProgram("windZoneProcedure");
			std::vector <int> N;
			N.push_back(particlesPositions.size());
			gpgpuEngine.SetProgramData(N, "N");
			gpgpuEngine.SetProgramData(particlesPositions, "position");
			gpgpuEngine.SetProgramData(particlesVelocity, "velocity");
			std::vector <glm::vec4> gv;
			gv.push_back(glm::vec4(gravityVelocity, 0.f));
			gpgpuEngine.SetProgramData(gv, "gravityVelocity");
			gpgpuEngine.SetProgramData(grid->windoNormals, "windVelocity");
			gpgpuEngine.SetProgramData(windIndexes, "windVelocityIndex");
			std::vector <float> windVel;
			windVel.push_back(grid->velocity);
			gpgpuEngine.SetProgramData(windVel, "windVel");
			std::vector <float> m;
			m.push_back(mass);
			gpgpuEngine.SetProgramData(m, "mass");
			std::vector <float> t;
			t.push_back(deltaTime);
			gpgpuEngine.SetProgramData(t, "time");
			std::vector <float> rc;
			rc.push_back(drag);
			gpgpuEngine.SetProgramData(rc, "resistanceDrag");
			std::vector <float> s;
			s.push_back(surface);
			gpgpuEngine.SetProgramData(s, "surface");
			std::vector <float> d;
			d.push_back(envirnoment->dencity);
			gpgpuEngine.SetProgramData(d, "dencity");


			gpgpuEngine.SetProgramData(particlesPositions, "outPosition");
			gpgpuEngine.SetProgramData(particlesVelocity, "outVelocity");
			gpgpuEngine.Work(N[0]);
			gpgpuEngine.GetProgramData(particlesPositions, "outPosition");
			gpgpuEngine.GetProgramData(particlesVelocity, "outVelocity");

			gpgpuEngine.ReleaseData();
		}
		catch (GPGPUerror error)
		{
			std::cout << error.description << std::endl;
		}
	}
	else
	{
		for (int i = 0; i < count; i++)
		{
			particlesLifeTime[i] -= deltaTime;
			int index = grid->GetCellIndex(particlesPositions[i]);
			if (index >= 0)
			{
				glm::vec4 velocity = grid->windoNormals[index] * grid->velocity;
				float value = envirnoment->dencity * surface * drag * 0.5f;
				glm::vec4 force = velocity * velocity * value;
				if (velocity != glm::vec4(0.f))
					force *= glm::normalize(velocity);
				glm::vec4 windVelocity = (force * deltaTime) / mass;
				particlesVelocity[i] = particlesVelocity[i] + glm::vec4(gravityVelocity, 0.f) + (envirnoment->AerodynamicDrag(drag, surface, &particlesVelocity[i]) * deltaTime);
				particlesPositions[i] += (particlesVelocity[i] + windVelocity) * deltaTime;
			}
			else
			{
				particlesVelocity[i] = particlesVelocity[i] + glm::vec4(gravityVelocity, 0.f) + (envirnoment->AerodynamicDrag(drag, surface, &particlesVelocity[i]) * deltaTime);
				particlesPositions[i] += particlesVelocity[i] * deltaTime;
			}
		}
	}

	if (CanEmitParticle())
		EmitParticle();

	SendToRender();

	Uint32 endUpdateTime = SDL_GetTicks();
	Uint32 updateTimeInt = endUpdateTime - startUpdateTime;
	double updateTimeDouble = (double)updateTimeInt / (double)1000;
	updateTime = (float)updateTimeInt;
}