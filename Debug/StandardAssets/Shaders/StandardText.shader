<header>
#version 330 core

uniform mat4 PM;
uniform mat4 MVM;

<vertex>
void main()
{
	gl_Position = MVM * vec4(0.0, 0.0, 0.0, 1.0);
}
</vertex>

<geometry>
layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

uniform vec4 rect;

out vec2 UV;

void main()
{
	vec4 position = gl_in[0].gl_Position;
	
	vec2 pos = position.xy + vec2(rect.x, -rect.w);
	gl_Position = PM * vec4(pos, position.zw);
	UV = vec2(0.0, 1.0);
    EmitVertex();
	
	pos = position.xy + vec2(rect.z, -rect.w);
	gl_Position = PM * vec4(pos, position.zw);
	UV = vec2(1.0, 1.0);
    EmitVertex();
	
	pos = position.xy + vec2(rect.x, rect.y);
	gl_Position = PM * vec4(pos, position.zw);
	UV = vec2(0.0, 0.0);
    EmitVertex();
	
	pos = position.xy + vec2(rect.z,  rect.y);
	gl_Position = PM * vec4(pos, position.zw);
	UV = vec2(1.0, 0.0);
    EmitVertex();
    EndPrimitive();
}
</geometry>

<fragment>
in vec2 UV;

uniform sampler2D textTexture;
uniform vec4 textColor;

out vec4 outColor;

void main()
{
	vec4 texel = texture(textTexture, UV);
	outColor = texel * textColor;
}
</fragment>