#pragma once

#include "ShaderParser.h"

class Shader
{
protected:
	GLuint programID;
	std::string path;
	std::vector <ShaderElement> uniformList;

	void LoadSource(std::string path, std::vector <ShaderInfo> *shaderinfo);
	void CompileShaders(std::vector <ShaderInfo> *shaderInfo);
	void CreateProgram(std::vector <ShaderInfo> *shaderInfo);
	void DeleteShaders(std::vector <ShaderInfo> *shaderInfo);

	GLuint FindUniformLocation(std::string name);

public:

	Shader();
	virtual ~Shader();

	void Init(std::string path);
	void Use();
	void TurnOff();
	void Delete();

	void Send(std::string name, glm::mat4 *matrix);
	void Send(std::string name, glm::vec4 *vector);
	void Send(GLuint location, glm::vec4 *vector);
	void Send(std::string name, glm::vec3 vector);
	void Send(GLuint location, glm::vec3 vector);
	void Send(std::string name, glm::vec2 vector);
	void Send(GLuint location, glm::vec2 vector);
	void Send(std::string name, float value);
	void Send(GLuint location, float value);
	void Send(std::string name, int value);
	void Send(GLuint location, int value);

	void SetPath(std::string path) { this->path = path; }

	virtual void FindeUniforms();

	std::string GetPath(){ return path; }

	ShaderElement *GetUniformData(int num);

	unsigned int GetUniformListSize() { return uniformList.size(); }
};

