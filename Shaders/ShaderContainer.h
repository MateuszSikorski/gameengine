#pragma once

#include "Header.h"
#include "Shader.h"

class ShaderContainer
{
private:
	std::vector<Shader*> shaders;
	Shader *LoadShader(std::string path);

public:
	ShaderContainer();
	virtual ~ShaderContainer();

	void DeleteAllShaders();
	void TurnOffShaders();
	void DeleteUnusedShader();//TODO

	Shader *GetShader(std::string path);
};

