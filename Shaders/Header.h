#pragma once

#include <string>
#include <sstream>
#include <vector>
#include <fstream>
#include <regex>

#include <C:\Users\Mateusz\Documents\BitBucket\GameEngine v2.0\UsefulElements\UsefulElements.h>

#include <H:\ConsoleApplication2\include\SDL.h>
#include <H:\ConsoleApplication2\include\glew.h>
#include <H:\ConsoleApplication2\include\SDL_opengl.h>
#include <H:\ConsoleApplication2\include\SDL_image.h>

#include <H:\ConsoleApplication2\include\glm\glm.hpp>
#include <H:\ConsoleApplication2\include\glm\gtc\quaternion.hpp>
#include <H:\ConsoleApplication2\include\glm\gtx\quaternion.hpp>
#include <H:\ConsoleApplication2\include\glm\gtx\transform.hpp>
#include <H:\ConsoleApplication2\include\glm\gtc\matrix_transform.hpp>
#include <H:\ConsoleApplication2\include\glm\gtc\random.hpp>

struct ShaderInfo
{
	GLenum type;
	GLuint shaderID;
	std::string source;

	ShaderInfo()
	{
		type = GL_NONE;
		shaderID = 0;
	}
};

enum ShaderElementType
{
	SE_NO_TYPE,
	SE_INT,
	SE_FLOAT,
	SE_VEC2,
	SE_VEC3,
	SE_VEC4,
	SE_MAT3,
	SE_MAT4,
	SE_TEXTURE2D
};

struct ShaderElement
{
	std::string name;
	ShaderElementType type;
	GLuint location;
};