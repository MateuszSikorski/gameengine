#include "ShaderParser.h"
#include <iostream>

ShaderParser::ShaderParser()
{
	pointer = NONE;
	v = f = g = false;
}

ShaderParser::~ShaderParser()
{
}

bool ShaderParser::CheckHeaderElement(std::string line)
{
	if (line == "<header>")
	{
		pointer = HEADER;
		return true;
	}
	else if (line == "<vertex>")
	{
		pointer = VERTEX;
		v = true;
		return true;
	}
	else if (line == "<geometry>")
	{
		pointer = GEOMETRY;
		g = true;
		return true;
	}
	else if (line == "<fragment>")
	{
		pointer = FRAGMENT;
		f = true;
		return true;
	}
	else if (line == "</vertex>" || line == "</geometry>" || line == "</fragment>")
	{
		pointer = NONE;
		return true;
	}

	return false;
}

ShaderElementType ShaderParser::GetElementType(std::string element)
{
	if (element == "int")
		return SE_INT;
	else if (element == "float")
		return SE_FLOAT;
	else if (element == "vec2")
		return SE_VEC2;
	else if (element == "vec3")
		return SE_VEC3;
	else if (element == "vec4")
		return SE_VEC4;
	else if (element == "mat3")
		return SE_MAT3;
	else if (element == "mat4")
		return SE_MAT4;
	else if (element == "sampler2D")
		return SE_TEXTURE2D;
	else
		return SE_NO_TYPE;
}

void ShaderParser::ParseUniform(std::string line, std::vector <ShaderElement> *elementList)
{
	std::regex pattern("\\s*uniform\\s*(\\w*)\\s*(\\w*)\\s*;");
	std::smatch parsedElement;
	if (std::regex_search(line, parsedElement, pattern))
	{
		ShaderElement element;
		element.type = GetElementType(parsedElement[1]);
		element.name = parsedElement[2];
		elementList->push_back(element);
	}
}

void ShaderParser::AddLine(std::string line)
{
	if (pointer == HEADER)
	{
		vertex += line + "\n";
		fragment += line + "\n";
		geometric += line + "\n";
	}
	else if(pointer == VERTEX)
		vertex += line + "\n";
	else if (pointer == FRAGMENT)
		fragment += line + "\n";
	else if (pointer == GEOMETRY)
		geometric += line + "\n";
}

void ShaderParser::CreateShader(std::vector <ShaderInfo> &shaderInfo)
{
	ShaderInfo info;
	if (v)
	{
		info.type = GL_VERTEX_SHADER;
		info.source = vertex;
		shaderInfo.push_back(info);
	}
	
	if (f)
	{
		info.type = GL_FRAGMENT_SHADER;
		info.source = fragment;
		shaderInfo.push_back(info);
	}

	if (g)
	{
		info.type = GL_GEOMETRY_SHADER;
		info.source = geometric;
		shaderInfo.push_back(info);
	}
}

void ShaderParser::Parse(std::vector <std::string> &code, std::vector <ShaderInfo> &shaderInfo, std::vector <ShaderElement> *elementList)
{
	for (auto line : code)
	{
		if (CheckHeaderElement(line))
			continue;

		ParseUniform(line, elementList);
		AddLine(line);
	}

	CreateShader(shaderInfo);
}