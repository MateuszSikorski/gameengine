#include "Shader.h"

Shader::Shader()
{
	programID = glCreateProgram();
}

Shader::~Shader()
{
	Delete();
}

void Shader::Delete()
{
	glDeleteProgram(programID);
}


void Shader::LoadSource(std::string path, std::vector <ShaderInfo> *shaderInfo)
{
	std::fstream file;
	file.open(path + ".shader");
	if (!file.is_open())
		throw "Could not finde file " + path;
	std::string line;
	std::vector <std::string> code;
	while (getline(file, line))
		code.push_back(line);

	file.close();

	ShaderParser parser;
	parser.Parse(code, *shaderInfo, &uniformList);
}

void Shader::CompileShaders(std::vector <ShaderInfo> *shaderInfo)
{
	for (auto &item : *shaderInfo)
	{
		item.shaderID = glCreateShader(item.type);
		const char *source = item.source.c_str();
		glShaderSource(item.shaderID, 1, &source, NULL);
		glCompileShader(item.shaderID);

		int length = 0;
		GLchar log[1024];
		glGetShaderInfoLog(item.shaderID, 1024, &length, log);
		
		if (log[0] != NULL)
			throw "---------\n" + path + ": " + "\n\n" + (std::string)log + "---------\n";
	}
}

void Shader::CreateProgram(std::vector <ShaderInfo> *shaderInfo)
{
	for (auto item : *shaderInfo)
		glAttachShader(programID, item.shaderID);

	glLinkProgram(programID);

	for (auto item : *shaderInfo)
		glDetachShader(programID, item.shaderID);
}

void Shader::DeleteShaders(std::vector <ShaderInfo> *shaderInfo)
{
	for (auto &item : *shaderInfo)
		glDeleteShader(item.shaderID);
}

void Shader::Init(std::string path)
{
	this->path = path;
	std::vector <ShaderInfo> shaderInfo;

	try
	{
		LoadSource(path, &shaderInfo);
		CompileShaders(&shaderInfo);
		CreateProgram(&shaderInfo);
		DeleteShaders(&shaderInfo);
	}
	catch (std::string error)
	{
		DeleteShaders(&shaderInfo);
		throw error;
	}

	FindeUniforms();
}

void Shader::Use()
{
	glUseProgram(programID);
}

void Shader::TurnOff()
{
	glUseProgram(0);
}

void Shader::FindeUniforms()
{
	for (int i = 0; i < uniformList.size(); i++)
	{
		uniformList[i].location = glGetUniformLocation(programID, uniformList[i].name.c_str());
	}
}

GLuint Shader::FindUniformLocation(std::string name)
{
	GLuint location = UINT_MAX;
	for (auto uniform : uniformList)
	{
		if (uniform.name == name)
		{
			location = uniform.location;
		}
	}

	return location;
}

void Shader::Send(std::string name, glm::mat4 *matrix)
{
	GLuint location = FindUniformLocation(name);
	if (location != UINT_MAX)
	{
		glm::mat4 m = *matrix;
		glUniformMatrix4fv(location, 1, GL_FALSE, &m[0][0]);
	}
}

void Shader::Send(std::string name, glm::vec4 *vector)
{
	GLuint location = FindUniformLocation(name);
	if (location != UINT_MAX)
	{
		glm::vec4 v = *vector;
		glUniform4fv(location, 1, &v[0]);
	}
}

void Shader::Send(GLuint location, glm::vec4 *vector)
{
	if (location != UINT_MAX)
	{
		glm::vec4 v = *vector;
		glUniform4fv(location, 1, &v[0]);
	}
}

void Shader::Send(std::string name, glm::vec3 vector)
{
	GLuint location = FindUniformLocation(name);
	if (location != UINT_MAX)
	{
		glUniform3fv(location, 1, &vector[0]);
	}
}

void Shader::Send(GLuint location, glm::vec3 vector)
{
	if (location != UINT_MAX)
		glUniform3fv(location, 1, &vector[0]);
}

void Shader::Send(std::string name, glm::vec2 vector)
{
	GLuint location = FindUniformLocation(name);
	if (location != UINT_MAX)
	{
		glUniform2fv(location, 1, &vector[0]);
	}
}

void Shader::Send(GLuint location, glm::vec2 vector)
{
	if (location != UINT_MAX)
		glUniform2fv(location, 1, &vector[0]);
}

void Shader::Send(std::string name, float value)
{
	GLuint location = FindUniformLocation(name);
	if (location != UINT_MAX)
	{
		glUniform1f(location, value);
	}
}

void Shader::Send(GLuint location, float value)
{
	if (location != UINT_MAX)
		glUniform1f(location, value);
}

void Shader::Send(std::string name, int value)
{
	GLuint location = FindUniformLocation(name);
	if (location != UINT_MAX)
	{
		glUniform1i(location, value);
	}
}

void Shader::Send(GLuint location, int value)
{
	if (location != UINT_MAX)
		glUniform1i(location, value);
}

ShaderElement *Shader::GetUniformData(int num)
{
	if (num < uniformList.size())
		return &uniformList[num];

	return nullptr;
}