#pragma once

#include "Header.h"

class ShaderParser
{
private:
	enum TypePointer
	{
		NONE,
		VERTEX,
		FRAGMENT,
		GEOMETRY,
		HEADER
	};

	std::string vertex, fragment, geometric;
	bool v, f, g;

	TypePointer pointer;
	bool CheckHeaderElement(std::string line);
	ShaderElementType GetElementType(std::string element);
	void ParseUniform(std::string line, std::vector <ShaderElement> *elementList);
	void AddLine(std::string line);
	void CreateShader(std::vector <ShaderInfo> &shaderInfo);

public:
	ShaderParser();
	virtual ~ShaderParser();

	void Parse(std::vector <std::string> &code, std::vector <ShaderInfo> &shaderInfo, std::vector <ShaderElement> *elementList);
};

