#include "ShaderContainer.h"

ShaderContainer::ShaderContainer()
{
}

ShaderContainer::~ShaderContainer()
{
	DeleteAllShaders();
}

void ShaderContainer::DeleteAllShaders()
{
	TurnOffShaders();

	for (auto *shader : shaders)
		delete shader;

	shaders.clear();
}

void ShaderContainer::TurnOffShaders()
{
	glUseProgram(0);
}

void ShaderContainer::DeleteUnusedShader()
{
}

Shader *ShaderContainer::LoadShader(std::string path)
{
	for (auto *item : shaders)
	{
		if (item->GetPath() == path)
			return item;
	}

	Shader *shader = new Shader();
	try
	{
		shader->Init(path);
	}
	catch (std::string error)
	{
		Log(error);
	}
	if (shader != nullptr)
		shaders.push_back(shader);

	return shader;
}

Shader *ShaderContainer::GetShader(std::string path)
{
	return LoadShader(path);
}