#pragma once

#include "RenderObjectsHeader.h"

class RenderObject
{
protected:
	Material *material;

public:
	RenderObject();
	virtual ~RenderObject();

	void AttachMaterial(Material *mat){ material = mat; }

	Material *GetMaterial(){ return material; }

	virtual void Render(glm::mat4 *projectionMatrix, glm::mat4 *modelMatrix, glm::mat4 *viewMatrix) = 0;
};
