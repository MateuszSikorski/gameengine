#pragma once
#include "RenderObject.h"

class RenderParticles : public RenderObject
{
protected:
	GLuint vao;
	GLuint vbo;

	int particlesCount;
	std::vector <glm::vec4> *positions;
	float size;

	void SendParticles();
	void DeleateParticles();
	void Render();

public:
	RenderParticles();
	virtual ~RenderParticles();

	virtual void Render(glm::mat4 *projectionMatrix, glm::mat4 *modelMatrix, glm::mat4 *viewMatrix);

	void SetSize(float particleSize) { size = particleSize; }
	void SetPositions(std::vector <glm::vec4> *pos);
};

