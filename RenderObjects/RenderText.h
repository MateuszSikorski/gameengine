#pragma once
#include "RenderObject.h"

class RenderText : public RenderObject
{
protected:
	GLuint vao;
	Texture *textTexture;

public:
	glm::vec4 rect;
	glm::vec4 textColor;

	RenderText();
	virtual ~RenderText();

	void SetTexture(Texture *newTexture);

	virtual void Render(glm::mat4 *projectionMatrix, glm::mat4 *modelMatrix, glm::mat4 *viewMatrix);
};

