#pragma once
#include "RenderObject.h"
class RenderModel :
	public RenderObject
{
protected:
	Mesh *mesh;

public:
	RenderModel();
	virtual ~RenderModel();

	void AttachMesh(Mesh *mesh){ this->mesh = mesh; }

	virtual void Render(glm::mat4 *projectionMatrix, glm::mat4 *modelMatrix, glm::mat4 *viewMatrix);
};
