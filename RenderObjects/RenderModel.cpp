#include "RenderModel.h"

RenderModel::RenderModel() : RenderObject()
{
	mesh = nullptr;
}

RenderModel::~RenderModel()
{
}

void RenderModel::Render(glm::mat4 *projectionMatrix, glm::mat4 *modelMatrix, glm::mat4 *viewMatrix)
{
	if (material != nullptr)
	{
		Shader *shader = material->GetShader();
		if (shader != nullptr)
		{
			glm::mat4 modelViewMatrix = *viewMatrix * *modelMatrix;
			shader->Send("MVM", &modelViewMatrix);
			shader->Send("PM", projectionMatrix);

			if (mesh != nullptr)
				mesh->Render();
		}
	}
}