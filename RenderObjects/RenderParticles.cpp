#include "RenderParticles.h"

RenderParticles::RenderParticles()
{
	particlesCount = 0;
	positions = nullptr;
}

RenderParticles::~RenderParticles()
{
}

void RenderParticles::Render()
{
	SendParticles();
	glBindVertexArray(vao);
	glEnableVertexAttribArray(0);
	glDrawArrays(GL_POINTS, 0, particlesCount);
	glDisableVertexAttribArray(0);
	DeleateParticles();
}

void RenderParticles::Render(glm::mat4 *projectionMatrix, glm::mat4 *modelMatrix, glm::mat4 *viewMatrix)
{
	if (material != nullptr)
	{
		Shader *shader = material->GetShader();
		if (shader != nullptr)
		{
			glm::mat4 modelViewMatrix = *viewMatrix;
			shader->Send("particleSize", size);
			shader->Send("MVM", &modelViewMatrix);
			shader->Send("PM", projectionMatrix);

			Render();
		}
	}
}

void RenderParticles::SetPositions(std::vector <glm::vec4> *pos)
{
	positions = pos;
	particlesCount = pos->size();
}

void RenderParticles::SendParticles()
{
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);

	glBindVertexArray(vao);

	if (positions->size() > 0)
	{
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4) * positions->size(), &positions[0][0], GL_DYNAMIC_DRAW);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
	}
}

void RenderParticles::DeleateParticles()
{
	glDeleteBuffers(1, &vbo);
	glDeleteVertexArrays(1, &vao);
	particlesCount = 0;
}