#include "RenderText.h"

RenderText::RenderText()
{
	glGenVertexArrays(1, &vao);
}

RenderText::~RenderText()
{
	glDeleteVertexArrays(1, &vao);
}

void RenderText::SetTexture(Texture *newTexture)
{
	textTexture = newTexture;
}

void RenderText::Render(glm::mat4 *projectionMatrix, glm::mat4 *modelMatrix, glm::mat4 *viewMatrix)
{
	if (material != nullptr)
	{
		Shader *shader = material->GetShader();
		if (shader != nullptr)
		{
			glm::mat4 modelViewMatrix = *viewMatrix * *modelMatrix;
			shader->Send("rect", &rect);
			shader->Send("textColor", &textColor);
			shader->Send("textTexture", 0);
			textTexture->BindTexture(0);
			shader->Send("MVM", &modelViewMatrix);
			shader->Send("PM", projectionMatrix);
	
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glBindVertexArray(vao);
			glDrawArrays(GL_POINTS, 0, 1);
			glDisable(GL_BLEND);
		}
	}
}