#include "FontContainer.h"

FontContainer::FontContainer()
{
}

FontContainer::~FontContainer()
{
	DeleteFonts();
}

void FontContainer::DeleteFonts()
{
	for (auto *font : fonts)
		delete font;

	fonts.clear();
}

Font *FontContainer::LoadFont(std::string path)
{
	for (auto *item : fonts)
	{
		if (item->GetPath() == path)
			return item;
	}

	Font *font = new Font();
	try
	{
		font->Load(path);
	}
	catch (std::string error)
	{
		Log(error);
		delete font;
		return nullptr;
	}

	fonts.push_back(font);
	return font;
}

Font *FontContainer::GetFont(std::string path)
{
	return LoadFont(path);
}