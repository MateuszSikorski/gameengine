#include "PerspectiveCameraRT.h"
#include <iostream>

PerspectiveCameraRT::PerspectiveCameraRT()
{
}

PerspectiveCameraRT::~PerspectiveCameraRT()
{
}

void PerspectiveCameraRT::SetViewport(glm::ivec2 viewport)
{
	PerspectiveCamera::SetViewport(viewport);
}

void PerspectiveCameraRT::SetAspect()
{
	PerspectiveCamera::SetAspect();
}
void PerspectiveCameraRT::SetFOV(float value)
{
	PerspectiveCamera::SetFOV(value);
}

void PerspectiveCameraRT::SetNearPlane(float value)
{
	PerspectiveCamera::SetNearPlane(value);
}

void PerspectiveCameraRT::SetFarPlane(float value)
{
	PerspectiveCamera::SetFarPlane(value);
}

void PerspectiveCameraRT::RefreshSceneForRendering()
{
	frameBuffer->Bind();
	glViewport(0, 0, 512, 512);
	glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void PerspectiveCameraRT::PostRender()
{
	frameBuffer->Unbind();
}

glm::mat4 PerspectiveCameraRT::GetViewMatrix()
{
	return PerspectiveCamera::GetViewMatrix();
}

glm::mat4 PerspectiveCameraRT::GetProjectionMatrix()
{
	return PerspectiveCamera::GetProjectionMatrix();
}

void PerspectiveCameraRT::SetFocalLength(float value, bool left)
{
	if (value >= 0.f)
	{
		focal = value;
		float translate = spacing * 0.5f;

		if (left)
		{
			glm::vec3 forward(-translate, 0, -focal);
			forward = glm::normalize(forward);
			glm::vec3 toPoint(0, 0, -1);
			glm::vec3 v = glm::cross(toPoint, forward);
			float w = glm::sqrt((glm::pow(forward.length(), 2) * glm::pow(toPoint.length(), 2))) + glm::dot(toPoint, forward);
			glm::quat rot(w, v.x, v.y, v.z);
			transform.SetGlobalRotation(glm::normalize(rot));
		}
		else
		{
			glm::vec3 forward = glm::vec3(translate, 0, -focal);
			forward = glm::normalize(forward);
			glm::vec3 toPoint = glm::vec3(0, 0, -1);
			glm::vec3 v = glm::cross(toPoint, forward);
			float w = glm::sqrt((glm::pow(forward.length(), 2) * glm::pow(toPoint.length(), 2))) + glm::dot(toPoint, forward);
			glm::quat rot = glm::quat(w, v.x, v.y, v.z);
			transform.SetGlobalRotation(glm::normalize(rot));
		}
	}
}

void PerspectiveCameraRT::SetSpacing(float value, bool left)
{
	spacing = value;
	float translate = spacing * 0.5f;
	glm::vec3 pos = transform.GetGlobalPosition();
	if(left)
		transform.SetGlobalPosition(glm::vec3(-translate + pos.x, pos.y, pos.z));
	else
		transform.SetGlobalPosition(glm::vec3(translate + pos.x, pos.y, pos.z));
}