#pragma once
#include "EmissionPoint.h"
class EmmisionQuad : public EmissionPoint
{
public:
	glm::vec3 scale;

	EmmisionQuad();
	virtual ~EmmisionQuad();

	virtual glm::vec3 RandPosition();
	virtual glm::vec3 RandDirection();
};

