#pragma once
#include "Object.h"
#include "Font.h"
#include "Resources.h"

class Text : public Object
{
protected:
	std::string text;
	glm::vec4 rect;
	Texture textTexture;
	glm::vec4 color;

public:
	Font *font;

	Text();
	Text(Font *font);
	virtual ~Text();

	void SetText(std::string newText);
	void SetColor(glm::vec4 *newColor);

	glm::vec4 GetColor() { return color; }
	glm::vec4 GetRect() { return rect; }

	std::string GetText() { return text; }
};

