#pragma once

#include "CoreHeader.h"

class Object
{
protected:
	RenderObject *renderObject;
	bool active;

public:
	unsigned int layer;
	std::string tag;
	bool dontDestroy;
	Transform transform;

	Object();
	virtual ~Object();

	void SetActive(bool value) { active = value; }
	void SetRenderObject(RenderObject *object){ renderObject = object; }

	bool GetActive() { return active; }
	RenderObject* GetRenderObject(){ return renderObject; }
};

