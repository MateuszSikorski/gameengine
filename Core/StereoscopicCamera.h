#pragma once
#include "PerspectiveCamera.h"

class StereoscopicCamera : public PerspectiveCamera
{
protected:
	PerspectiveCamera left, right;
	bool renderLeft;
	float spacing;
	float focalLength;

public:
	StereoscopicCamera();
	virtual ~StereoscopicCamera();

	virtual void SetViewport(glm::ivec2 viewport);
	virtual void SetAspect();
	virtual void SetFOV(float value);
	virtual void SetNearPlane(float value);
	virtual void SetFarPlane(float value);
	virtual void RefreshSceneForRendering();
	virtual glm::mat4 GetViewMatrix();
	virtual glm::mat4 GetProjectionMatrix();
	virtual void PostRender();

	void SetSpacing(float value);
	void SetFocalLength(float value);

	float GetSpacing() { return spacing; }
	float GetFocalLength() { return focalLength; }
};

