#include "StereoscopicCamera.h"

StereoscopicCamera::StereoscopicCamera()
{
	renderLeft = false;
	transform.AddChildren(&left.transform);
	transform.AddChildren(&right.transform);
	SetSpacing(0.25f);
	SetFocalLength(100.f);
}

StereoscopicCamera::~StereoscopicCamera()
{
}

void StereoscopicCamera::SetNearPlane(float value)
{
	nearPlane = value;
	left.SetNearPlane(value);
	right.SetNearPlane(value);
}

void StereoscopicCamera::SetFarPlane(float value)
{
	farPlane = value;
	left.SetFarPlane(value);
	right.SetFarPlane(value);
}

void StereoscopicCamera::RefreshSceneForRendering()
{
	Camera::RefreshSceneForRendering();
	renderLeft = !renderLeft;
}

glm::mat4 StereoscopicCamera::GetViewMatrix()
{
	if (renderLeft)
		return left.GetViewMatrix();
	else
		return right.GetViewMatrix();
}

glm::mat4 StereoscopicCamera::GetProjectionMatrix()
{
	if (renderLeft)
		return left.GetProjectionMatrix();
	else
		return right.GetProjectionMatrix();
}

void StereoscopicCamera::SetAspect()
{
	aspect = (float)viewportWidth / (float)viewportHeight;
	left.SetAspect();
	right.SetAspect();
}

void StereoscopicCamera::SetFOV(float value)
{
	fov = value;
	left.SetFOV(value);
	right.SetFOV(value);
}

void StereoscopicCamera::SetViewport(glm::ivec2 viewport)
{
	Camera::SetViewport(viewport);
	left.SetViewport(viewport);
	right.SetViewport(viewport);
}

void StereoscopicCamera::SetSpacing(float value)
{
	spacing = value;
	float translate = spacing * 0.5f;
	left.transform.SetLocalPosition(glm::vec3(-translate, 0, 0));
	right.transform.SetLocalPosition(glm::vec3(translate, 0, 0));
}

void StereoscopicCamera::SetFocalLength(float value)
{
	if (value >= 0.f)
	{
		focalLength = value;
		float translate = spacing * 0.5f;

		glm::vec3 forward(-translate, 0, -focalLength);
		forward = glm::normalize(forward);
		glm::vec3 toPoint(0, 0, -1);
		glm::vec3 v = glm::cross(toPoint, forward);
		float w = glm::sqrt((glm::pow(forward.length(), 2) * glm::pow(toPoint.length(), 2))) + glm::dot(toPoint, forward);
		glm::quat rot(w, v.x, v.y, v.z);
		left.transform.SetLocalRotation(glm::normalize(rot));

		forward = glm::vec3(translate, 0, -focalLength);
		forward = glm::normalize(forward);
		toPoint = glm::vec3(0, 0, -1);
		v = glm::cross(toPoint, forward);
		w = glm::sqrt((glm::pow(forward.length(), 2) * glm::pow(toPoint.length(), 2))) + glm::dot(toPoint, forward);
		rot = glm::quat(w, v.x, v.y, v.z);
		right.transform.SetLocalRotation(glm::normalize(rot));
	}
}

void StereoscopicCamera::PostRender()
{
}