#pragma once
#include "CoreHeader.h"

class GlobalTime
{
private:
	Uint32 startTime;
	Uint32 endTime;

	GlobalTime();

public:
	virtual ~GlobalTime();

	static GlobalTime &GetInstance();

	void Start();
	void End();

	float GetTimeSinceStart();
	float DeltaTime();
};

