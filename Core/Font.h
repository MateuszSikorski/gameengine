#pragma once
#include "CoreHeader.h"

class Font
{
protected:
	TTF_Font *font;
	std::string path;
	int size;

public:
	Font();
	virtual ~Font();

	void Load(std::string path);
	void SetSize(int size);
	void Destroy();

	TTF_Font *GetFont() { return font; }

	int GetSize() { return size; }

	std::string GetPath() { return path; }
};

