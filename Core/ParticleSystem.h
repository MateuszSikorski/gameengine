#pragma once

#include "EmissionSolid.h"
#include "GameObject.h"

struct Particle
{
	glm::vec3 position;
	glm::vec3 velocity;
	float lifeTime;
};

class ParticleSystem : public GameObject
{
protected:
	std::vector <glm::vec4> particlesPositions;
	std::vector <glm::vec4> particlesVelocity;
	std::vector <float> particlesLifeTime;

	float timeSinceLastEmit;

	void SendToRender();
	void EmitParticle();
	bool CanEmitParticle();
	int GetIndexOfDeadParticle();

public:
	int maxParticles;
	float particleLifeTime;
	float emissionTimeOffset;
	float velocity;
	float particleSize;
	EmissionSolid *solid;

	ParticleSystem();
	virtual ~ParticleSystem();

	virtual void Update();
	virtual void PostUpdate();

	int GetParticlesCount() { return particlesPositions.size(); }
};

