#pragma once
#include "Object.h"
#include "GlobalTime.h"

class GameObject : public Object
{
private:

public:
	GameObject();
	virtual ~GameObject();

	virtual void Update() = 0;
	virtual void PostUpdate() = 0;
};

