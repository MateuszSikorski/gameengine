#include "Engine.h"

Engine::Engine()
{
	window = nullptr;
	width = height = 0;
	scene = nullptr;
}

Engine::~Engine()
{
	Destroy();
}

Engine &Engine::GetInstance()
{
	static Engine instance;
	return instance;
}

void Engine::Create(std::string title, int w, int h, bool fullscreen)
{
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
		throw (std::string)"Error on init";

	window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, w, h, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	if (window == nullptr)
		throw (std::string)"Error on window create";

	width = w;
	height = h;

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	renderContext = SDL_GL_CreateContext(window);
	if (!renderContext)
		throw (std::string)"Error on creating render context";

	glewExperimental = GL_TRUE;
	GLenum error = glewInit();
	if (error != GLEW_OK)
		throw (std::string)"Error on glew init";

	IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG | IMG_INIT_TIF);
	TTF_Init();

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE);
}

void Engine::Destroy()
{
	Resources::GetInstance().Destroy();
	DeleteScenes();
	SDL_GL_DeleteContext(renderContext);
	SDL_DestroyWindow(window);
	window = nullptr;
	scene = nullptr;
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}

glm::ivec2 Engine::GetViewport()
{
	return glm::ivec2(width, height);
}

void Engine::SelectObjectsToRender(Camera *camera, std::vector <Object*> *objectsToRender)
{
	for (auto layer : camera->layerList)
	{
		for (auto *obj : scene->objects)
		{
			if (obj->layer == layer && obj->GetActive())
				objectsToRender->push_back(obj);
		}
	}
}

#include <iostream>

void Engine::RenderObjects()
{
	for (auto *camera : scene->cameras)
	{
		if (!camera->GetActive())
			continue;

		camera->RefreshSceneForRendering();
		std::vector <Object*> objectsToRender;
		SelectObjectsToRender(camera, &objectsToRender);

		for (auto *object : objectsToRender)
		{
			RenderObject *renderObject = object->GetRenderObject();
			if (renderObject != nullptr)
			{
				Material *mat = renderObject->GetMaterial();
				if (mat)
				{
					mat->UseShader();
					mat->SendData();
				}

				renderObject->Render(&camera->GetProjectionMatrix(), &object->transform.GetModelMatrix(), &camera->GetViewMatrix());
			}
		}

		camera->PostRender();
	}
}

void Engine::UpdateGameObjects()
{
	for (auto *object : scene->objects)
	{
		GameObject *gameObject = dynamic_cast<GameObject*>(object);
		if (gameObject != nullptr)
		{
			if(gameObject->GetActive())
				gameObject->Update();
		}
	}
}

void Engine::PostUpdateGameObjects()
{
	for (auto *object : scene->objects)
	{
		GameObject *gameObject = dynamic_cast<GameObject*>(object);
		if (gameObject != nullptr)
		{
			if(gameObject->GetActive())
				gameObject->PostUpdate();
		}
	}
}

void Engine::Work()
{
	UpdateGameObjects();
	PostUpdateGameObjects();
	RenderWorld();
}

void Engine::RenderWorld()
{
	GlobalTime::GetInstance().Start();
	if (scene != nullptr)
	{
		RenderObjects();
	}

	SDL_GL_SwapWindow(window);
	GlobalTime::GetInstance().End();
	float deltaTime = GlobalTime::GetInstance().DeltaTime();
	float offsetTime = 0.016f - deltaTime;
	if (offsetTime > 0.f)
	{
		offsetTime *= 1000.f;
		SDL_Delay((UINT32)offsetTime);
		GlobalTime::GetInstance().End();
	}
}

Object *Engine::AddObject(Object *object)
{
	scene->AddObject(object);
	Camera *camera = dynamic_cast<Camera*>(object);
	if (camera != nullptr)
		scene->AddCamera(camera);
		
	return object;
}

void Engine::DeleteObjectsFromScene()
{
	scene->DeleteObjects();
}

void Engine::DeleteScenes()
{
	for (auto *scene : cachedScenes)
	{
		scene->Destroy();
		delete scene;
	}

	cachedScenes.clear();
}

void Engine::AddScene(Scene *scene)
{
	cachedScenes.push_back(scene);
}

void Engine::SetCurrentScene(int i)
{
	if (i >= 0 && i < cachedScenes.size())
		scene = cachedScenes[i];
}

void Engine::SetCurrentScene(std::string name)
{
	for (int i = 0; i < cachedScenes.size(); i++)
	{
		if (cachedScenes[i]->name == name)
			scene = cachedScenes[i];
	}
}

void Engine::DeleteScene(std::string name)
{
	for (int i = 0; i < cachedScenes.size(); i++)
	{
		if (cachedScenes[i]->name == name)
		{
			delete cachedScenes[i];
			cachedScenes.erase(cachedScenes.begin() + i);
		}
	}
}

