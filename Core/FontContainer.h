#pragma once
#include "Font.h"

class FontContainer
{
private:
	std::vector <Font*> fonts;

	Font *LoadFont(std::string path);

public:
	FontContainer();
	~FontContainer();

	void DeleteFonts();

	Font *GetFont(std::string path);
};

