#include "EmmisionQuad.h"

EmmisionQuad::EmmisionQuad()
{
	angle = 0.f;
	scale = glm::vec3(1,1,1);
}

EmmisionQuad::~EmmisionQuad()
{
}

glm::vec3 EmmisionQuad::RandPosition()
{
	glm::vec3 pos = glm::linearRand(glm::vec3(-1, -1, -1), glm::vec3(1, 1, 1));
	pos *= scale;
	return pos;
}

glm::vec3 EmmisionQuad::RandDirection()
{
	return EmissionPoint::RandDirection();
}