#include "GlobalTime.h"

GlobalTime::GlobalTime()
{
}

GlobalTime::~GlobalTime()
{
}

GlobalTime &GlobalTime::GetInstance()
{
	static GlobalTime instance;
	return instance;
}

void GlobalTime::Start()
{
	startTime = SDL_GetTicks();
}

void GlobalTime::End()
{
	endTime = SDL_GetTicks();
}

float GlobalTime::GetTimeSinceStart()
{
	Uint32 timei = SDL_GetTicks();
	double time = (double)timei;
	time *= 0.001;

	return time;
}

float GlobalTime::DeltaTime()
{
	Uint32 deltaTimei = endTime - startTime;
	double deltaTime = (double)deltaTimei;
	deltaTime *= 0.001;

	return (float)deltaTime;
}