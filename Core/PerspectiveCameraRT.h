#pragma once
#include "PerspectiveCamera.h"
#include "FrameBuffer.h"

class PerspectiveCameraRT : public PerspectiveCamera
{
protected:
	FrameBuffer *frameBuffer;
	float focal;
	float spacing;

public:
	PerspectiveCameraRT();
	virtual ~PerspectiveCameraRT();

	virtual void SetViewport(glm::ivec2 viewport);
	virtual void SetAspect();
	virtual void SetFOV(float value);
	virtual void SetNearPlane(float value);
	virtual void SetFarPlane(float value);
	virtual void RefreshSceneForRendering();
	virtual glm::mat4 GetViewMatrix();
	virtual glm::mat4 GetProjectionMatrix();
	virtual void PostRender();

	void SetFocalLength(float value, bool left);
	void SetSpacing(float value, bool left);
	void AttachFrameBuffer(FrameBuffer *fb) { frameBuffer = fb; }

	FrameBuffer *GetFrameBuffer() { return frameBuffer; }
};

