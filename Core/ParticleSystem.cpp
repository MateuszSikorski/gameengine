#include "ParticleSystem.h"

ParticleSystem::ParticleSystem()
{
	maxParticles = 100;
	particleLifeTime = 2.f;
	emissionTimeOffset = 1.f;
	velocity = 1.f;
	timeSinceLastEmit = 0.f;
	particleSize = 1.f;
	solid = nullptr;
}

ParticleSystem::~ParticleSystem()
{
	delete solid;
}

void ParticleSystem::Update()
{
}

int ParticleSystem::GetIndexOfDeadParticle()
{
	int count = particlesLifeTime.size();
	for (int i = 0; i < count; i++)
	{
		if (particlesLifeTime[i] < 0.f)
			return i;
	}

	return -1;
}

bool ParticleSystem::CanEmitParticle()
{	
	bool deadParticle = false;
	if (GetIndexOfDeadParticle() > -1)
		deadParticle = true;

	if (timeSinceLastEmit >= emissionTimeOffset && (particlesPositions.size() < maxParticles || deadParticle))
	{
		timeSinceLastEmit = 0.f;
		return true;
	}
	else
	{
		timeSinceLastEmit += GlobalTime::GetInstance().DeltaTime();
		return false;
	}
}

void ParticleSystem::SendToRender()
{
	RenderParticles *renderer = dynamic_cast<RenderParticles*>(renderObject);
	renderer->SetPositions(&particlesPositions);
	renderer->SetSize(particleSize);
	/*if (renderer != nullptr)
	{
		std::vector <glm::vec3> pos;
		int count = particlesLifeTime.size();
		renderer->SetSize(particleSize);
		renderer->SetPositions(pos);
	}*/
}

void ParticleSystem::PostUpdate()
{
	float deltaTime = GlobalTime::GetInstance().DeltaTime();
	int count = particlesPositions.size();
	for (int i = 0; i < count; i++)
	{
		particlesLifeTime[i] -= deltaTime;
		particlesPositions[i] += particlesVelocity[i] * deltaTime;
	}

	if (CanEmitParticle())
		EmitParticle();

	SendToRender();
}

void ParticleSystem::EmitParticle()
{
	if (solid != nullptr)
	{
		int count = 1;
		float deltaTime = GlobalTime::GetInstance().DeltaTime();
		if (emissionTimeOffset < deltaTime)
			count = (int)(deltaTime / emissionTimeOffset);

		for (int i = 0; i < count; i++)
		{
			Particle newParticle;
			newParticle.position = solid->RandPosition() + transform.GetGlobalPosition();
			newParticle.velocity = (transform.GetGlobalRotation() * solid->RandDirection()) * velocity;
			newParticle.lifeTime = particleLifeTime;

			int index = GetIndexOfDeadParticle();
			if (index > -1)
			{
				particlesPositions[index] = glm::vec4(newParticle.position, 1.f);
				particlesVelocity[index] = glm::vec4(newParticle.velocity, 0.f);
				particlesLifeTime[index] = newParticle.lifeTime;
			}
			else
			{
				particlesPositions.push_back(glm::vec4(newParticle.position, 1.f));
				particlesVelocity.push_back(glm::vec4(newParticle.velocity, 0.f));
				particlesLifeTime.push_back(newParticle.lifeTime);
			}
		}
	}
}