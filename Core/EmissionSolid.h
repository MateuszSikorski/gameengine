#pragma once
#include "CoreHeader.h"

class EmissionSolid
{
public:
	EmissionSolid();
	virtual ~EmissionSolid();

	virtual glm::vec3 RandPosition() = 0;
	virtual glm::vec3 RandDirection() = 0;
};

