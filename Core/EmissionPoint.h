#pragma once
#include "EmissionSolid.h"

class EmissionPoint : public EmissionSolid
{
protected:
	float angle;

public:
	EmissionPoint();
	virtual ~EmissionPoint();

	virtual glm::vec3 RandPosition();
	virtual glm::vec3 RandDirection();

	void SetAngle(float angle);
};

