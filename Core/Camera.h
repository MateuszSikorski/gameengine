#pragma once

#include "CoreHeader.h"
#include "Object.h"

enum CameraClearFlag
{
	COLOR_DEPTH,
	DEPTH,
	NOTHING
};

class Camera : public Object
{
protected:
	glm::mat4 projectionMatrix, viewMatrix;
	float nearPlane, farPlane;

public:
	CameraClearFlag clearFlag;
	glm::vec4 clearColor;
	float viewportWidth, viewportHeight;
	std::vector <unsigned int> layerList;

	Camera();
	virtual ~Camera();

	virtual void SetViewport(glm::ivec2 viewport);
	virtual void SetNearPlane(float value) = 0;
	virtual void SetFarPlane(float value) = 0;
	virtual void RefreshSceneForRendering();
	virtual glm::mat4 GetViewMatrix();
	virtual glm::mat4 GetProjectionMatrix();
	virtual void PostRender() = 0;

	float GetNearPlane() { return nearPlane; }
	float GetFarPlane() { return farPlane; }

	void AddLayer(unsigned int layer);
	void RemoveLayer(unsigned int layer);
};
