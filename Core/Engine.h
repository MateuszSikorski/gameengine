#pragma once

#include "CoreHeader.h"
#include "Scene.h"
#include "Resources.h"
#include "GlobalTime.h"
#include "PerspectiveCamera.h"
#include "OrthographicCamera.h"
#include "GameObject.h"
#include "ParticleSystem.h"
#include "EmissionPoint.h"
#include "Text.h"
#include "StereoscopicCamera.h"
#include "EmmisionQuad.h"
#include "PerspectiveCameraRT.h"
#include "FrameBuffer.h"

/*Singleton class*/

class Engine
{
private:
	SDL_Window *window;
	SDL_GLContext renderContext;
	int width, height;

	std::vector<Scene*> cachedScenes;
	Scene *scene;

	Engine();

	void SelectObjectsToRender(Camera *camera, std::vector <Object*> *objectsToRender);
	void RenderObjects();
	void UpdateGameObjects();
	void PostUpdateGameObjects();

public:
	virtual ~Engine();

	static Engine &GetInstance();

	void Create(std::string title, int w, int h, bool fullscreen);
	void Destroy();
	void RenderWorld();
	void Work();

	Object *AddObject(Object *object);
	void DeleteObjectsFromScene();
	void DeleteScenes();
	void DeleteScene(std::string name);
	void AddScene(Scene *scene);
	void SetCurrentScene(int i);
	void SetCurrentScene(std::string name);

	int GetWidth(){ return width; }
	int GetHeight(){ return height; }

	glm::ivec2 GetViewport();
};

