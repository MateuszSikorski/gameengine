#pragma once

#include "Camera.h"

class OrthographicCamera :
	public Camera
{
public:
	float top, bottom, left, right;

	OrthographicCamera();
	virtual ~OrthographicCamera();

	void SetTop(float value);
	void SetBottom(float value);
	void SetLeft(float value);
	void SetRight(float value);

	virtual void SetViewport(glm::ivec2 viewport);
	virtual void SetNearPlane(float value);
	virtual void SetFarPlane(float value);
	virtual void RefreshSceneForRendering();
	virtual glm::mat4 GetViewMatrix();
	virtual glm::mat4 GetProjectionMatrix();
	virtual void PostRender();
};
