#include "Scene.h"

Scene::Scene()
{
}

Scene::Scene(std::string name)
{
	this->name = name;
}

Scene::~Scene()
{
	Destroy();
}

void Scene::Destroy()
{
	DeleteObjects();
}

void Scene::AddObject(Object *object)
{
	objects.push_back(object);
}

void Scene::AddCamera(Camera *camera)
{
	cameras.push_back(camera);
}

void Scene::DeleteObjects()
{
	for (auto *object : objects)
	{
		if(!object->dontDestroy)
			delete object;
	}

	objects.clear();
	cameras.clear();
}
