#include "Resources.h"

Resources::Resources()
{
	materials.AttachShaderCountainer(&shaders);
	materials.AttachTextureCountainer(&textures);
}

Resources::~Resources()
{
	Destroy();
}

Resources &Resources::GetInstance()
{
	static Resources instance;
	return instance;
}

void Resources::Destroy()
{
	fonts.DeleteFonts();
	materials.DeleteMaterials();
	shaders.DeleteAllShaders();
	textures.DeleteTextures();
	meshes.DeleteMeshes();
}

Shader *Resources::GetShader(std::string path)
{
	return shaders.GetShader(path);
}

Material *Resources::GetMaterial(std::string path)
{
	return materials.GetMaterial(path);
}

Texture *Resources::GetTexture(std::string path)
{
	return textures.GetTexture(path);
}

Mesh *Resources::GetMesh(std::string path)
{
	return meshes.GetMesh(path);
}

Font *Resources::GetFont(std::string path)
{
	return fonts.GetFont(path);
}