#pragma once

#include "Camera.h"

class PerspectiveCamera :
	public Camera
{
protected:
	float aspect, fov;

public:

	PerspectiveCamera();
	virtual ~PerspectiveCamera();

	virtual void SetViewport(glm::ivec2 viewport);
	virtual void SetAspect();
	virtual void SetFOV(float value);
	virtual void SetNearPlane(float value);
	virtual void SetFarPlane(float value);
	virtual void RefreshSceneForRendering();
	virtual glm::mat4 GetViewMatrix();
	virtual glm::mat4 GetProjectionMatrix();
	virtual void PostRender();

	float GetAspect() { return aspect; }
	float GetFOV() { return fov; }
};

