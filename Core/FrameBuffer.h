#pragma once
#include "CoreHeader.h"

class FrameBuffer
{
protected:
	GLuint frameBuffer;
	Texture renderTexture;

	void InitBuffer();

public:
	FrameBuffer();
	virtual ~FrameBuffer();

	void Bind();
	void Unbind();

	Texture *GetRenderTexture() { return &renderTexture; }
};

