#pragma once

#include "CoreHeader.h"
#include "FontContainer.h"

class Resources
{
private:
	ShaderContainer shaders;
	MaterialContainer materials;
	TextureContainer textures;
	MeshContainer meshes;
	FontContainer fonts;

	Resources();

public:
	virtual ~Resources();

	static Resources &GetInstance();

	void Destroy();

	Shader *GetShader(std::string path);
	Material *GetMaterial(std::string path);
	Texture *GetTexture(std::string path);
	Mesh *GetMesh(std::string path);
	Font *GetFont(std::string path);
};
