#include "Camera.h"

#include <iostream>
Camera::Camera()
{
	layerList.push_back(0);
	clearFlag = COLOR_DEPTH;
	clearColor = glm::vec4(0,0,0,0);
	viewportWidth = viewportHeight = 1;
}

Camera::~Camera()
{
}

void Camera::AddLayer(unsigned int layer)
{
	for (auto item : layerList)
	{
		if (item == layer)
			return;
	}

	layerList.push_back(layer);
}

void Camera::RemoveLayer(unsigned int layer)
{
	for (int i = 0; i < layerList.size(); i++)
	{
		if (layerList[i] == layer)
			layerList.erase(layerList.begin() + i);
	}
}

void Camera::SetViewport(glm::ivec2 viewport)
{
	viewportWidth = viewport.x;
	viewportHeight = viewport.y;
}

void Camera::RefreshSceneForRendering()
{
	glViewport(0, 0, viewportWidth, viewportHeight);
	switch (clearFlag)
	{
	case COLOR_DEPTH:
		glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		break;
	case DEPTH:
	{
		GLfloat depth[] = { 1,1,1,1 };
		glClearBufferfv(GL_DEPTH, 0, depth);
	}
	case NOTHING:
		break;
	}
}

glm::mat4 Camera::GetViewMatrix()
{
	viewMatrix = glm::translate(glm::mat4(), -transform.GetGlobalPosition());
	viewMatrix *= glm::mat4_cast(transform.GetGlobalRotation());

	return viewMatrix;
}

glm::mat4 Camera::GetProjectionMatrix()
{
	return projectionMatrix;
}