#include "EmissionPoint.h"

EmissionPoint::EmissionPoint()
{
	angle = glm::radians(15.f);
}

EmissionPoint::~EmissionPoint()
{
}

glm::vec3 EmissionPoint::RandPosition()
{
	return glm::vec3(0.f);
}

void EmissionPoint::SetAngle(float angle)
{
	this->angle = glm::radians(angle);
}

glm::vec3 EmissionPoint::RandDirection()
{
	float random = glm::linearRand(0.f, 1.f);
	random *= 6.2832f;
	float randomAngle = glm::linearRand(0.f, 1.f);
	randomAngle *= angle;

	glm::vec3 direction;
	direction.y = glm::cos(randomAngle);
	direction.x = glm::sin(randomAngle) * glm::cos(random);
	direction.z = glm::sin(randomAngle) * glm::sin(random);

	return direction;
}