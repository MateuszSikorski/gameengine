#include "OrthographicCamera.h"

OrthographicCamera::OrthographicCamera()
{
	right = top = 1.f;
	left = bottom = -1.f;
	nearPlane = -10.f;
	farPlane = 10.f;
	projectionMatrix = glm::ortho(left, right, bottom, top, nearPlane, farPlane);
}

OrthographicCamera::~OrthographicCamera()
{
}

void OrthographicCamera::SetTop(float value)
{
	top = value;
	projectionMatrix = glm::ortho(left, right, bottom, top, nearPlane, farPlane);
}

void OrthographicCamera::SetBottom(float value)
{
	bottom = value;
	projectionMatrix = glm::ortho(left, right, bottom, top, nearPlane, farPlane);
}

void OrthographicCamera::SetLeft(float value)
{
	left = value;
	projectionMatrix = glm::ortho(left, right, bottom, top, nearPlane, farPlane);
}

void OrthographicCamera::SetRight(float value)
{
	right = value;
	projectionMatrix = glm::ortho(left, right, bottom, top, nearPlane, farPlane);
}

void OrthographicCamera::SetNearPlane(float value)
{
	nearPlane = value;
	projectionMatrix = glm::ortho(left, right, bottom, top, nearPlane, farPlane);
}

void OrthographicCamera::SetFarPlane(float value)
{
	farPlane = value;
	projectionMatrix = glm::ortho(left, right, bottom, top, nearPlane, farPlane);
}

void OrthographicCamera::RefreshSceneForRendering()
{
	Camera::RefreshSceneForRendering();
}

glm::mat4 OrthographicCamera::GetViewMatrix()
{
	return Camera::GetViewMatrix();
}

glm::mat4 OrthographicCamera::GetProjectionMatrix()
{
	return Camera::GetProjectionMatrix();
}

void OrthographicCamera::SetViewport(glm::ivec2 viewport)
{
	viewportWidth = viewport.x;
	viewportHeight = viewport.y;
}

void OrthographicCamera::PostRender()
{
}