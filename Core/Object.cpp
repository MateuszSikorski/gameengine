#include "Object.h"

Object::Object()
{
	renderObject = nullptr;
	layer = 0;
	dontDestroy = false;
	active = true;
}

Object::~Object()
{
	delete renderObject;
}