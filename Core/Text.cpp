#include "Text.h"

Text::Text()
{
	renderObject = new RenderText();
	renderObject->AttachMaterial(Resources::GetInstance().GetMaterial("StandardAssets/Materials/StandardText.mat"));
	rect.x = 0.f;
	rect.y = 0.f;
	SetText("Text");
	SetColor(&glm::vec4(1,1,1,1));
}

Text::Text(Font *font)
{
	renderObject = new RenderText();
	renderObject->AttachMaterial(Resources::GetInstance().GetMaterial("StandardAssets/Materials/StandardText.mat"));
	this->font = font;
	rect.x = 0.f;
	rect.y = 0.f;
	SetText("Text");
	SetColor(&glm::vec4(1, 1, 1, 1));
}

Text::~Text()
{
}

void Text::SetColor(glm::vec4 *newColor)
{
	color = *newColor;
	RenderText *renderer = dynamic_cast<RenderText*>(renderObject);
	renderer->textColor = color;
}

void Text::SetText(std::string newText)
{
	text = newText;
	SDL_Color textColor = {255, 255, 255};
	SDL_Surface *surface = TTF_RenderText_Blended(font->GetFont(), text.c_str(), textColor);
	textTexture.SetData(surface);
	rect.z = surface->w;
	rect.w = surface->h;

	RenderText *renderer = dynamic_cast<RenderText*>(renderObject);
	renderer->SetTexture(&textTexture);
	renderer->rect = rect;
	renderer->textColor = color;
}