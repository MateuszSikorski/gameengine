#pragma once

#include "CoreHeader.h"
#include "Camera.h"

class Scene
{
public:
	std::string name;
	std::vector <Object*> objects;
	std::vector <Camera*> cameras;

	Scene();
	Scene(std::string name);
	virtual ~Scene();

	void AddObject(Object *object);
	void AddCamera(Camera *camera);
	void DeleteObjects();

	void Destroy();
};

