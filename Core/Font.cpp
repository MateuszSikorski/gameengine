#include "Font.h"

Font::Font()
{
	font = nullptr;
	size = 12;
}

Font::~Font()
{
	Destroy();
}

void Font::Destroy()
{
	TTF_CloseFont(font);
	font = nullptr;
}

void Font::Load(std::string path)
{
	if (font != nullptr)
		Destroy();

	font = TTF_OpenFont(path.c_str(), size);
	this->path = path;
	if (font == NULL)
	{
		std::string error = "Error on load: " + path;
		throw error;
	}
}

void Font::SetSize(int size)
{
	this->size = size;
	Load(path);
}