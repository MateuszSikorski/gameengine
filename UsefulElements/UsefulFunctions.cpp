#include "UsefulFunctions.h"

void StringSplit(std::string *data, char dispacher, std::vector <std::string> *out)
{
	std::string tempString;
	std::stringstream stream(*data);
	while (getline(stream, tempString, dispacher))
		out->push_back(tempString);
}

std::string VectorToString(glm::vec2 vec)
{
	std::string value = std::to_string(vec.x) + " ";
	value += std::to_string(vec.y);

	return value;
}

std::string VectorToString(glm::vec3 vec)
{
	std::string value = std::to_string(vec.x) + " ";
	value += std::to_string(vec.y) + " ";
	value += std::to_string(vec.z);

	return value;
}

std::string VectorToString(glm::vec4 *vec)
{
	std::string value = std::to_string(vec->x) + " ";
	value += std::to_string(vec->y) + " ";
	value += std::to_string(vec->z) + " ";
	value += std::to_string(vec->w);

	return value;
}

glm::vec2 StringToVector2(std::string *data)
{
	std::vector <std::string> split;
	StringSplit(data, ' ', &split);
	if (split.size() == 2)
		return glm::vec2(std::stof(split[0]), std::stof(split[1]));

	return glm::vec2(0, 0);
}

glm::vec3 StringToVector3(std::string *data)
{
	std::vector <std::string> split;
	StringSplit(data, ' ', &split);
	if (split.size() == 3)
		return glm::vec3(std::stof(split[0]), std::stof(split[1]), std::stof(split[2]));

	return glm::vec3(0, 0, 0);
}

glm::vec4 StringToVector4(std::string *data)
{
	std::vector <std::string> split;
	StringSplit(data, ' ', &split);
	if (split.size() == 4)
		return glm::vec4(std::stof(split[0]), std::stof(split[1]), std::stof(split[2]), std::stof(split[3]));

	return glm::vec4(0, 0, 0, 0);
}

void ClearLog()
{
	std::ofstream file("log.txt");
	file.clear();
	file.close();
}

void Log(std::string message)
{
	std::ofstream file("log.txt", std::ios::app | std::ios::ate);
	message += "\n";
	file.write(message.c_str(), message.size());
	file.close();
}