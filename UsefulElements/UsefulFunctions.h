#pragma once

#include "Header.h"
void StringSplit(std::string *data, char dispacher, std::vector <std::string> *out);
void Log(std::string message);
void ClearLog();

std::string VectorToString(glm::vec2 vec);
std::string VectorToString(glm::vec3 vec);
std::string VectorToString(glm::vec4 *vec);

glm::vec2 StringToVector2(std::string *data);
glm::vec3 StringToVector3(std::string *data);
glm::vec4 StringToVector4(std::string *data);
